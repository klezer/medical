import { Component } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { ClientService, FirebaseService, NotificationService, UtilsService } from "./../../../services";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as dialogs from "tns-core-modules/ui/dialogs";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { FingerprintAuth, BiometricIDAvailableResult } from "nativescript-fingerprint-auth";

@Component({
    selector: "Home",
    moduleId: module.id,
    styleUrls: ['./home.component.scss'],
    templateUrl: "./home.component.html",
    providers: [FirebaseService]
})
export class HomeComponent {
    public form: any;
    public isAndroid = isAndroid;
    public isIOS = isIOS;
    public submitted = false;
    public not_show_password = true;   
    public kepp_login: boolean;
    public loadedAsync = false;
    private fingerprintAuth: FingerprintAuth;
    public btnFngerprintAuth = false;
    public displayOption = {
      login: true,
      forgetLogin: false,
      confirmAccountPassword: false,
      newPassword: false
    };
    public routers = {
      login: "api-login",
      forgotPassword: "send-email-reset",
      validateSms: "validate-code-reset",
      resetPassword: "password-reset"
    };

    constructor(public page: Page,
                private clientService: ClientService,
                private notificationService: NotificationService,
                private firebaseService: FirebaseService,
                private utilsService: UtilsService) {
        this.page.actionBarHidden = true;
        this.fingerprintAuth = new FingerprintAuth();
        if((ApplicationSettings.hasKey('registrationSteps'))){
            this.utilsService.goToScreen('/register-image');
        } else {
            this.clientService.isLogged().then(() => {
                this.utilsService.goToScreen('/dashboard');
            });
        }
    }

  ngAfterContentInit(): void {
      this.formControl('login');
      this.loadedAsync = true;
  }

  /**
   * Define todo os campos do formuário
   * com suas regras de validação.
   */
  private formControl(type) {
    let form = {
      email: new FormControl(""),
      password: new FormControl(""),
      code: new FormControl("")
    };

    if (type == "login") {
      form.email = new FormControl("", [Validators.required, Validators.email]);
      form.password = new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]);
    }
    if (type == "code") {
      form.code = new FormControl("", [
        Validators.required,
        Validators.minLength(5)
      ]);
    }
    if (type == "reset") {
      form.password = new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]);
    }
    this.form = new FormGroup(form);
  }

  /**
   * Realiza o login do usuário.
   */
  public submitLogin() {
      this.utilsService.dismissSoftKeybaord();
      this.submitted = true;
      this.firebaseService.getCurrentPushToken().then((response: any) => {
          ApplicationSettings.setString('deviceToken', response);
          if(this.form.valid){
              this.login(this.form.value);
          }
      });
  }


  /**
   * Recebe os dados e realiza o login.
   */
  public login(login){
      login.device_token = ApplicationSettings.getString('deviceToken');
      this.clientService.postAll('api-login', login, true, ['error']).then((response: any) => {
          this.submitted = false;
          ApplicationSettings.setString(
              "possibleFingerprintAuth",
              JSON.stringify(login)
          );
          if(response.success) {
            this.isDoctor(response);
          }
      }).catch(() => {
        this.submitted = false;
      });
  }

  /**
   * Realiza a verificação se realmente é um profissional.
   */
  public isDoctor(response) {
      if(response.success.is_doctor){
          this.clientService.setApiServiceTokenApi(response.success.token);
          ApplicationSettings.setString('userProfile',JSON.stringify(response.success));
          this.utilsService.goToScreen('/dashboard');
      } else {
          this.notificationService.showFeedback({
              type: "error",
              message: 'O seu cadastro só permite acesso ao App de Paciente.'
          });
      } 
  }


  /**
   * Inicia a recuperação da senha do usuário.
   */
  public recoveryPassword() {
    const form = {
      email: this.form.get("email").value
    };
    this.submitted = true;
    this.clientService
      .postAll(this.routers.forgotPassword, form, false, ["error"])
      .then((response: any) => {
        this.submitted = false;
        if (response.success) {
          ApplicationSettings.setNumber("recoveryID", response.success.user_id),
            this.changeScreen("confirmAccountPassword");
          this.formControl("code");
        }
      });
  }

  /**
   * Realiza a validação do código de recuperação de senha.
   */
  public validateCode() {
    const form = {
      user_id: ApplicationSettings.getNumber("recoveryID"),
      code: this.form.get("code").value
    };
    this.submitted = true;
    this.clientService
      .postAll(this.routers.validateSms, form, false, ["error"])
      .then((response: any) => {
        this.submitted = false;
        if (response.success) {
          this.changeScreen("newPassword");
          this.formControl("reset");
        }
      });
  }

  /**
   * Define um nova senha.
   */
  public redefinePassword() {
    const form = {
      user_id: ApplicationSettings.getNumber("recoveryID"),
      password: this.form.get("password").value
    };
    this.submitted = true;
    this.clientService
      .postAll(this.routers.resetPassword, form, false, ["error"])
      .then((response: any) => {
        this.submitted = false;
        if (response.success) {
          this.notificationService.showFeedback({
            type: "success",
            message: "Senha alterada com sucesso!"
          });
          this.changeScreen("login");
        }
      });
  }

  /**
   * forgetPassword
   */
  public forgetPassword() {
    dialogs
      .prompt({
        title: "RECUPERAÇÃO DE SENHA",
        message: "Informe o seu e-mail para iniciar o processo.",
        okButtonText: "Recuperar",
        cancelButtonText: "Cancelar",
        inputType: dialogs.inputType.email
      })
      .then(r => {
        console.log("Dialog result: " + r.result + ", text: " + r.text);
      })
      .catch(error => console.log(error));
  }

  /**
   * Altera a visualização desejada.
   */
  public changeScreen(screen) {
    Object.keys(this.displayOption).forEach(view => {
      this.displayOption[view] = false;
    });
    this.displayOption[screen] = true;
  }

  /**
   * Verifica se o usuário pode utilizar a
   * digital para os próximos acesso.
   */
  public useFingerprintAuth() {
    if(ApplicationSettings.hasKey("possibleFingerprintAuth")) {
      this.fingerprintAuth
      .available()
      .then((result: BiometricIDAvailableResult) => {
        this.btnFngerprintAuth = result.any;
      });
    }
  }

  /**
   * Aciona a opção de login automático do dispositivo.
   */
  public useFingerPrint() {
    this.fingerprintAuth
      .verifyFingerprint({
        title: "Biometria",
        message: "Verificando o acesso",
        authenticationValidityDuration: 10,
        useCustomAndroidUI: false
      })
      .then((enteredPassword?: string) => {
        if (!enteredPassword) {
          const login = JSON.parse(
            ApplicationSettings.getString("possibleFingerprintAuth")
          );
          this.login(login);
        }
      })
      .catch(() =>
        this.notificationService.showFeedback({
          type: "error",
          message:
            "Biometria não reconhecida, informe os seus dados de acesso."
        })
      );
  }

}
