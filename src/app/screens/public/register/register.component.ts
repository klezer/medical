import { Component, AfterContentInit } from "@angular/core";
import { GoogleAutoCompleteService, ClientService, NotificationService, UtilsService } from "./../../../services";
import { Page } from "tns-core-modules/ui/page";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValueList, SelectedIndexChangedEventData } from "nativescript-drop-down";
import { TextField } from "tns-core-modules/ui/text-field";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import { topmost } from "tns-core-modules/ui/frame";
import * as moment from 'moment';
import * as ApplicationSettings from "tns-core-modules/application-settings";

@Component({
    selector: 'Register',
    moduleId: module.id,
    templateUrl: "./register.component.html",
    styleUrls: ["./register.component.scss"]
})

export class RegisterComponent implements AfterContentInit {
    public delayTimer: any;
    public topmost = topmost;
    public isAndroid = isAndroid;
    public isIOS = isIOS;
    public formRegister: any;
    public submitted: boolean = false;
    public inProccess: boolean = false;
    public genres: ValueList<string>;
    public FormGenres: any;
    public specialties: ValueList<string>;
    public FormSpecialties: any;
    public advices: ValueList<string>;
    public FormAdvices: any;
    public states: ValueList<string>;
    public FormStates: any;
    public citys: ValueList<string>;
    public FormCitys: any;
    public Address: any;
    public select_state_value: number = 0;
    public select_city_value: number = 0;
    public select_genre_value: number = 0;
    public select_speciality_value: number = 0;
    public select_region_value: number = 0;
    public select_advice_value: number = 0;
    public loadedAsync: boolean = false;
    public loadedLists: boolean = false;
    public not_show_password: boolean = true;
    public moment = moment;
    public rule = /[^a-z0-9]/gi;
    public routers = {
        registerUser: 'doctor-cad-create',
        listCitys: 'doctor-cad-citys',
        listAdvices: 'doctor-list-advice',
        listSpecialties: 'doctor-list-specialty'
    }

    constructor(private googleAutoCompleteService: GoogleAutoCompleteService,
                private clientService: ClientService,
                private notificationService: NotificationService,
                private utilsService: UtilsService,
                public page: Page){       
        this.page.actionBarHidden = true;
        /** Cidades */  
        this.FormCitys = [
            { value: "", display: "Cidade"}
        ];
        this.citys =  new ValueList<string>(this.FormCitys);
        /** Estados */  
        this.FormStates = [
            { value: "", display: "UF"}
        ];
        this.states =  new ValueList<string>(this.FormStates);
        /** Gêneros */  
        this.FormGenres = [
            { value: "", display: "Gênero"},
            { value: "F", display: "Feminino" },
            { value: "M", display: "Masculino" }
        ];
        this.genres =  new ValueList<string>(this.FormGenres);
        /** Atualiza os estados */
        this.clientService.getAll('doctor-cad-states', null, ['error']).then(response => {
            this.FormStates = response;
            this.FormStates.splice(0, 0, { value: "", display: "UF"});
            this.states =  new ValueList<string>(this.FormStates);
        });  
        /** Especialidade */  
        this.FormSpecialties = [
            { value: "", display: "Especialidade"}
        ];
        this.specialties =  new ValueList<string>(this.FormSpecialties);
        /** Conselho */  
        this.FormAdvices = [
            { value: "", display: "Conselho" }
        ];
        this.advices =  new ValueList<string>(this.FormAdvices);
        // Atualiza os conselhos
        this.clientService.getAll(this.routers.listAdvices, null, ['error']).then(response => {
            this.FormAdvices = response;
            this.FormAdvices.splice(0, 0, { value: "", display: "Conselho"});
            this.advices =  new ValueList<string>(this.FormAdvices);
            /** Conclui a atualização das listas */
            this.loadedLists = true;
        });
    }

    ngAfterContentInit(): void {
        this.formControl();
        this.loadedConfirm();
    }
 
     /**
      * confirmação de carregamento da API.
      */
     public loadedConfirm() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.loadedAsync = true;
        }, 450);
     }

    /**
     * Define todo os campos do formuário 
     * com suas regras de validação.
     */
    private formControl() {
        this.formRegister = new FormGroup({
            name: new FormControl('', [Validators.required]),
            advice_id: new FormControl('', [Validators.required]),
            region_id: new FormControl('', [Validators.required]),
            specialty_id: new FormControl('', [Validators.required]),
            crm_number: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required, Validators.email]),
            telephone_one: new FormControl('', [Validators.required]),
            cpf: new FormControl('', [Validators.required]),
            rg: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required, Validators.minLength(6)]),
            password_confirmation: new FormControl('', [Validators.required, Validators.minLength(6)]),
            locality: new FormControl('', [Validators.required]),
            street_code: new FormControl('', [Validators.required]),
            street_title: new FormControl('', [Validators.required]),
            number: new FormControl('', [Validators.required]),
            state_id: new FormControl('', [Validators.required]),
            complement: new FormControl(''),
            city_id: new FormControl('', [Validators.required]),
            district_title: new FormControl('', [Validators.required]),
            genre: new FormControl('', [Validators.required]),
            document_professional: new FormControl('upload', [Validators.required]),
            date_of_birth: new FormControl('', [Validators.required])
        });
    }

    /**
     * Recebe o valor do campo e insere no formulário.
     */
    public getValue(args, field) {
        this.formRegister.controls[field].setValue((<TextField>args.object).text);
    }

    /**
     * Pega o endereco API do Google Maps.
     */
    public getAddressByCep(args) {
        let onlyNumbers = (<TextField>args.object).text.replace(/[^0-9]/g,'');
        // Verifica se é númerico e possui a quantidade necessária para pesquisa
        if(/\d/.test(onlyNumbers) && onlyNumbers.length >= 7){
            this.googleAutoCompleteService.getAddressByInfo(onlyNumbers, true).then((response) => {
                const address = this.googleAutoCompleteService.filterAddress(response);
                this.autoCompleteAddressFiels(address.street_title, address.district_title, address.mycity, address.state);
            });
         }
    }

    /**
     * Insere os dados nos campos de endeço.
     */
    public autoCompleteAddressFiels(street_title, district_title, mycity, state) {
        // define a rua
        if(street_title){
            this.formRegister.controls['street_title'].setValue(street_title);
        }
        // define o bairro
        if(district_title){
            this.formRegister.controls['district_title'].setValue(district_title);
        }
        // define o estado
        this.select_state_value = this.FormStates.map(item => item.display).indexOf(state);
        this.formRegister.controls['state_id'].setValue(this.FormStates[this.select_state_value].value);
        this.autoCompleteCity(this.select_state_value).then(() => {
            this.select_city_value = this.FormCitys.map(item => item.display).indexOf(mycity);
            this.formRegister.controls['city_id'].setValue(this.FormCitys[this.select_city_value].value);
        });
    }
          
    /**
     * Observa as mudanças do componete de selecão e atribui os valores.
     */
    public changeSelect(args: SelectedIndexChangedEventData, field) {
        // Para estado
        if(field === 'state_id'){
            this.autoCompleteCity(args.newIndex);
            this.formRegister.controls[field].setValue(this.FormStates[args.newIndex].value);
            this.select_state_value = args.newIndex;
        }
        // Para Conselho
        else if( field === 'advice_id'){
            this.autoCompleteSpecialty(args.newIndex);
            this.formRegister.controls[field].setValue(this.FormAdvices[args.newIndex].value);
            this.select_advice_value = args.newIndex;
        }
        // Para Conselho
        else if( field === 'region_id'){
            this.formRegister.controls[field].setValue(this.FormStates[args.newIndex].value);
            this.select_region_value = args.newIndex;
        }
        // Para gênero
        else if( field === 'genre'){
            this.formRegister.controls[field].setValue(this.FormGenres[args.newIndex].value);
        }
        // Para cidade
        else if( field === 'city_id'){
            this.formRegister.controls[field].setValue(this.FormCitys[args.newIndex].value);
            this.select_city_value = args.newIndex;
        }
        // Para especialidade
        else if( field === 'specialty_id'){
            this.formRegister.controls[field].setValue(this.FormSpecialties[args.newIndex].value);
        }
        
    }

    /**
     * Realiza o autocomplete do estado/cidade.
     */
    public autoCompleteCity(state) {
        return new Promise((resolve) => {
            this.clientService.getById(this.routers.listCitys, this.FormStates[state].value).then((response: any) => {
                this.FormCitys = response.citys;
                this.FormCitys.splice(0, 0, { value: '', display: 'Selecione'});
                this.citys = new ValueList<string>(this.FormCitys);
                resolve(state);
            });
        });
    }

    /**
     * Realiza o autocomplete da especialidade.
     */
    public autoCompleteSpecialty(advice) {
        return new Promise((resolve) => {
            this.clientService.getById(this.routers.listSpecialties, this.FormAdvices[advice].value).then((response: any) => {
                this.FormSpecialties = response;
                this.FormSpecialties.splice(0, 0, { value: '', display: 'Selecione'});
                this.specialties = new ValueList<string>(this.FormSpecialties);
                resolve(advice);
            });
        });
    }

    /**
     * Define valores padrões para o campo de cidade.
     */
    public changeSelectClosed(field, field_id) {
        this[field] = 0;
        this.formRegister.controls[field_id].setValue();
    }

    /**
     * Realiza o cadastro do novo usuário.
     */
    public registerUser() {
        this.utilsService.dismissSoftKeybaord();
        this.submitted = true;
        let formatData = this.formatData(this.formRegister.value)
        if(this.formRegister.get('password').value == this.formRegister.get('password_confirmation').value){
            this.inProccess = true;
            this.clientService.postAll(this.routers.registerUser, formatData, true, ['error']).then((response: any) => {
                this.inProccess = false;
                // próximo passo do cadastro
                if(response.success){
                    /** Uso exclusivo para a vínculo da foto */
                    ApplicationSettings.setNumber('userID',response.id);
                    /** uso exclusivo para o primeiro acesso após o cadastro */
                    ApplicationSettings.setString('firstAccess_email',formatData.email);
                    ApplicationSettings.setString('firstAccess_password',formatData.password);
                    this.utilsService.goToScreen('/register-image');
                }
            })
        }
    }

    /**
     * Formata os dados do formulário.
     */
    public formatData(value) {
        // Remove as máscaras
        let rule = /[^a-z0-9]/gi;
        let form = JSON.stringify(value);
        let newForm = JSON.parse(form);
        let formRegister = newForm;
        // Valida a data
        if(!this.formRegister.get('date_of_birth').invalid && this.formRegister.get('date_of_birth') != null){
            let date = newForm['date_of_birth'].split('/');
            let year = date[2].replace(rule, '');
            let month = date[1].replace(rule, '');
            let day = date[0].replace(rule, '');
            if (year.length == 4 && (month.length && day.length)  == 2){
                formRegister.date_of_birth = year + '-' + month + '-'+ day;
            } else {
                this.notificationService.showFeedback({
                    type: "warning",
                    message: 'Data de nascimento inválido.'
                  });
                return false;
            }
        }
        // Valida outros campos
        formRegister.telephone_one = (newForm['telephone_one'] != null) ? newForm['telephone_one'].replace(rule, '') : '';
        formRegister.cpf = (newForm['cpf'] != null) ? newForm['cpf'].replace(rule, '') : '';
        formRegister.street_code = (newForm['street_code'] != null) ? newForm['street_code'].replace(rule, '') : '';
        // Retorna os valores tratados
        return formRegister;
    }
 
}

