import { Component, ViewContainerRef } from "@angular/core";
import { ClientService } from "./../../../services";
import { isAndroid } from "tns-core-modules/platform";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { screen } from "tns-core-modules/platform";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as moment from 'moment'; 
import 'moment/min/locales';
/** Ações da Receita */
import { StartTratmentComponent } from "./../recipes/actions";

@Component({
    selector: "Recipes",
    moduleId: module.id,
    templateUrl: "./recipes.component.html",
    styleUrls: ['./recipes.component.scss']
})

export class RecipesComponent {
    public isAndroid = isAndroid;
    public screen = screen;
    public recipes = [];
    public loadedAsync: boolean = false;
    public userProfile: any;
    public routers = {
        retrieveAll: 'doctor-list-recipe'
    }
    public delayTimer: any = null;
    public currentPage = 1;
    public monthList = [];
    public rule = '[0-9]{0,2}\/[0-9]{0,2}\/[0-9]{0,4}';
    public displayOption = {
        emptyList: true,
        hasList: false
    }

    constructor(public page: Page,
                private clientService: ClientService,
                private vcRef: ViewContainerRef,
                private modal: ModalDialogService){
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.onDrawerButtonTap(false);
        this.getUserInfo();
    }

    ngAfterContentInit(): void {
        this.loadedConfirm();
    }
    
    /** 
     * Abre/fecha menu  
     */
    public onDrawerButtonTap(confirm): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        if (sideDrawer !== undefined && sideDrawer.getIsOpen()){
            sideDrawer.closeDrawer();
        } if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm){
            sideDrawer.showDrawer();
        }
    }

    /**
     * confirmação de carregamento da API.
     */
    public loadedConfirm() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.getAll(false);
            this.allowFingerPrint();
          }, 450);
    }

    /**
     * Verifica se deve permitir a biometria.
     */
    public allowFingerPrint() {
        if(ApplicationSettings.hasKey('possibleFingerprintAuth')){
            ApplicationSettings.setString(
                "useFingerprintAuth",
                ApplicationSettings.getString("possibleFingerprintAuth")
            );
        }
    }

  /**
   * Verifica e pega as informações do usuário recém logado.
   */
    public getUserInfo() {
        if (ApplicationSettings.hasKey("userProfile")) {
            this.userProfile = JSON.parse(
                ApplicationSettings.getString("userProfile")
            );
        }
    }
    
  /**
   * Pega os itens.
   */
    public getAll(search) {
        this.clientService
        .getAllPaginate(
            this.routers.retrieveAll,
            this.userProfile.id,
            this.currentPage,
            ["error"],
            search
        )
        .then((response: any) => {
            this.loadedAsync = true;
            if (response.length > 0) {
                this.changeScreen('hasList');
                this.currentPage = this.currentPage + 1;
                response.forEach(item => {
                    this.filterMonth(item);
                    this.recipes.push(item);
                });
            }
        });
    }

    /**
     * Pagina mais itens.
     */
    public loadMore() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.getAll(false);
        }, 1000);
    }

    /**
     * Realiza a consulta do termo bucado.
     */
    public search(searchInfo) {
        this.loadedAsync = false;
        this.currentPage = 1;
        this.recipes = [];
        this.monthList = [];
        this.getAll(searchInfo.length > 0 ? searchInfo : false);
    }

    /**
     * Exibe os modais de acordo com as ação desejada pelo usuário.
     */
    public addRecipes() {
        let options: ModalDialogOptions = {
            context: { },
            fullscreen: false,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(StartTratmentComponent, options);
    }

    /**
     * Realiza a verificação de meses e os agrupa.
     */
    public filterMonth(item) {
        var isValid = new RegExp(this.rule);
        if(isValid){
            let finalMoment = this.formatMonth(item);
            if(!this.monthList.filter(item => item.date == finalMoment).length) {
                this.monthList.push({ id: item.recipe_id, date: finalMoment });
            }
        }
    }

    /**
     * Formata o mês para exibição.
     */
    public formatMonth(item) {
        let finalDate = item.date_generator;
        return moment(finalDate).lang('pt-br').format('MMMM YYYY');
    }

    /**
     * Realiza a verificação do mês para sua exibição.
     */
    public visibleMonth(recipe_id) {
        return this.monthList.filter(item => item.id === recipe_id).length;
    }
  
  /**
   * Altera a visualização desejada.
   */
  public changeScreen(screen) {
    Object.keys(this.displayOption).forEach(view => {
      this.displayOption[view] = false;
    });
    this.displayOption[screen] = true;
  }
}
