import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { RecipesRoutingModule } from "./recipes-routing.module";
import { RecipesComponent } from "./recipes.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
/** Diretivas */
import { RecipeItemModule } from './../../../directives/recipe-item/recipe-item.module';
import { SearchTopBarModule } from './../../../directives/search-top-bar/search-top-bar.module';

@NgModule({
    imports: [
        RecipeItemModule,
        SearchTopBarModule,
        NativeScriptCommonModule,
        RecipesRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        RecipesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        RecipesComponent
    ]
})
export class RecipesModule { }
