import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { StartTratmentComponent } from "./start-tratment.component";

const routes: Routes = [
    { path: "", component: StartTratmentComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class StartTratmentRoutingModule { }
