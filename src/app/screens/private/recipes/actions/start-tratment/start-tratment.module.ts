import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { StartTratmentRoutingModule } from "./start-tratment-routing.module";
import { StartTratmentComponent } from "./start-tratment.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptFormsModule } from "nativescript-angular/forms";

@NgModule({
    imports: [
        ReactiveFormsModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        StartTratmentRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        StartTratmentComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class StartTratmentModule { }
