import { Component } from "@angular/core";
import { ClientService } from "./../../../../../services";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import { RouterExtensions } from "nativescript-angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TextField } from "tns-core-modules/ui/text-field";

@Component({
    selector: "StartTratment",
    moduleId: module.id,
    templateUrl: "./start-tratment.component.html",
    styleUrls: ['./start-tratment.component.scss']
})

export class StartTratmentComponent { 
    public isAndroid = isAndroid;
    public isIOS = isIOS;
    public form: FormGroup;
    public CPF: string;
    public messageError: string;
    public patient: any;
    public submitted: boolean = false;
    public inProccess: boolean = false;
    public loadForm = false;

    public constructor(
            private params: ModalDialogParams,
            private clientService: ClientService,
            private RouterExtensions: RouterExtensions) {
            this.patient = null;
            this.messageError = null;
            this.formControl();
    }

    /**
     * Define todo os campos do formuário 
     * com suas regras de validação.
     */
    private formControl() {
        /** Definição do formulário */
        this.form = new FormGroup({
            cpf: new FormControl('', [Validators.required, Validators.minLength(11)])
        });
    }

    /**
     * Recebe o valor do campo e insere no formulário.
     */
    public getValue(args, field) {
        this.form.controls[field].setValue((<TextField>args.object).text);
    }

    /**
     * Pesquisa o CPF do paciente.
     */
    public searchCPF() {
        this.submitted = true;
        let rule = /[^a-z0-9]/gi;
        let cpf = this.form.get('cpf').value.replace(rule, '');
        this.form.controls['cpf'].setValue(cpf);
        /** Valida o formuário */
        if(this.form.valid){
            this.inProccess = true;
            /** Realiza a consulta */
            this.clientService
                .getAll('doctor-search-user-cpf', this.form.get('cpf').value, ['error'])
                    .then((response: any) => {
                        this.submitted = false;
                        this.inProccess = false;
                        this.showPatient(response.personal.user_id);
            });
        }
    }

    /** Redireciona para a tela de emissão de receita */
    public showPatient(patientID) {
        this.close();
        // necessário para fechar o modal
        setTimeout(() => {
            this.RouterExtensions.navigate([`/new-recipe/${patientID}`], { 
                clearHistory: true,
                transition: {
                    name: "fade",
                }
            });
        }, 50);
    }

    /**
     * Fecha o modal.
     */
    public close() {
        this.params.closeCallback();
    }

}
