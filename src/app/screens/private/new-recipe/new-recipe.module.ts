import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NewRecipeRoutingModule } from "./new-recipe-routing.module";
import { NewRecipeComponent } from "./new-recipe.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
/** Diretivas */
import { ContactUserModule } from './../../../directives/contact-user/contact-user.module';
import { MedicineItensModule } from './../../../directives/medicine-itens/medicine-itens.module';
@NgModule({
    imports: [
        ContactUserModule,
        MedicineItensModule,
        NativeScriptFormsModule,
        ReactiveFormsModule,
        NativeScriptCommonModule,
        NewRecipeRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        NewRecipeComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        NewRecipeComponent
    ]
})
export class NewRecipeModule { }
