import { ActivatedRoute } from '@angular/router';
import { Component, ViewChild, ElementRef } from "@angular/core";
import { ClientService, EventEmitterService, NotificationService } from "./../../../services";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { screen } from "tns-core-modules/platform";
import { topmost } from "tns-core-modules/ui/frame";
import { RouterExtensions } from "nativescript-angular/router";
import { FingerprintAuth, BiometricIDAvailableResult } from "nativescript-fingerprint-auth";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as email from 'nativescript-email';

@Component({
    selector: "NewRecipe",
    moduleId: module.id,
    templateUrl: "./new-recipe.component.html",
    styleUrls: ['./new-recipe.component.scss']
})

export class NewRecipeComponent {
    public delayTimer: any;
    public isAndroid = isAndroid;
    public isIOS = isIOS;
    public userProfile: any;
    public urlParameter: any;
    public submitted = false;
    public inProccess = false;
    public loadedAsync;

    public userPatient: any = null;
    public patientID: any = null;
    public recipeID: any = null;

    public formRecipe: any = [];
    public listMedicine: any = [];
    public formMedicine: any = [];
    public itemMedicine: any = {
        dosage: null,
        considerations: null,
        frequency: null,
        number_of_days: null,
        title: null,
        subtitle: null,
        note: '',
        validity_of_medicine: null,
        opened: true
    };
    public loadedLists = false;
    public medicineType = 1;
    public screen = screen;
    public topmost = topmost;
    public currentValue = 10;
    public fontSize = 20;
    public canBack;
    public canCreateRecipe = false;
    public routers = {
        retrieveRecipe: 'doctor-recipe-patient',
        createRecipe: 'doctor-create-recipe',
        interaction_medical: 'doctor-interaction-medical'
    }
    private fingerprintAuth: FingerprintAuth;
    public interactionList = [];
    @ViewChild('terminalScroller', { static: false }) private terminalScroller: ElementRef;
    constructor(private ActivatedRoute: ActivatedRoute,
        private clientService: ClientService,
        private notificationService: NotificationService,
        public page: Page,
        private routerExtensions: RouterExtensions){         
        page.actionBarHidden = true;
        this.fingerprintAuth = new FingerprintAuth();
        this.userProfile = JSON.parse(ApplicationSettings.getString('userProfile'));
         /** Recebe os parâmetros da rota */
         this.ActivatedRoute.params.forEach((params) => {
            this.patientID = params.patient_id;
            this.recipeID = params.recipe_id;
        });
        /** Formulário de comentário */
        this.formControl();
        this.monitorMedicineItens();
    }

    ngAfterContentInit(): void {
        this.loadedConfirm();
    }

    /**
     * confirmação de carregamento da API.
     */
    public loadedConfirm() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.getPatient();
            if(this.recipeID != null){
                this.getRecipeInfo();
            } else {
                this.listMedicine.push(this.itemMedicine);
            }
        }, 450);
    }

    /**
     * Monitora a lista de medicamentos e percorre o scroll ao elemento.
     */
    public monitorMedicineItens() {
        EventEmitterService.get("event-medicine-list").subscribe({
            next: () => {
                this.terminalScroller.nativeElement.scrollToVerticalOffset(35, true);
            }
        });
    }

    /**
     * Pega os dados do paciente.
     */
    public getPatient() {
        this.clientService.getById('doctor-get-user-id', this.patientID).then((response: any) => {
            this.userPatient = {
                name: response.personal.name,
                image: response.picture.image,
                email: response.personal.email,
                telephone: response.contact.telephone_one
            };
            /** Informa o carregamento do conteúdo */
            this.loadedAsync = true;
        });
    }

    /**
     * Pega as informações da receita.
     */
    public getRecipeInfo() {
        this.clientService.getById(this.routers.retrieveRecipe, this.recipeID).then((response: any) => {
            response.medicines.forEach((item) => {
                item.opened = false;
                item.selected = true;
                item.titulo = item.title;
                item.subtitulo = item.note;
                item.subtitle = item.note;
                item.medicine_id = item.id;
                item.note = item.subtitle;
                this.listMedicine.push(item);
            });
        });
    }

    /**
     * Adiciona o novo item do formulário validado.
     * @param form 
     */
    public medicineForm(form) {
        if (form.new){
            this.canCreateRecipe = true;
            this.formRecipe[form.index] = form.value;
            this.listMedicine[form.index].medicine_id = form.value.medicine_id;
            this.drugInteraction(form.for_interaction);
        }
        if (!form.new){
            this.formRecipe.splice(form.index, 1);
            this.listMedicine.splice(form.index, 1);
            this.interactionList.splice(form.index, 1);
        }
    }

    /**
     * Valida o preenchimento do medicamento.
     */
    public validRecipe(valid) {
        this.canCreateRecipe = valid;
    }

    /**
     * Define todo os campos do formuário 
     * com suas regras de validação.
     */
    private formControl() {
        /** Formulário Geral de Medicamentos */
        this.formMedicine = new FormGroup({
            user_id: new FormControl(this.patientID, [Validators.required]),
            doctor_id: new FormControl(this.userProfile.id, [Validators.required]),
            type_recipe_id: new FormControl(this.medicineType, [Validators.required]),
        });
    }

    /** Submete uma nova receita */
    public newRecipe() {
        this.submitted = true;
        let formMedicine = this.formMedicine.value;
        let formRecipe = this.formRecipe;
        let newForm = formMedicine;
        newForm.medicines = formRecipe;
        /** Valida o formuário */
        if(this.formMedicine.valid) {
            this.inProccess = true;
            this.clientService.postAll(this.routers.createRecipe, newForm, false, ['error']).then((response: any) => {
                if (response.success) {
                    this.routerExtensions.navigate(["/dashboard"], {
                        clearHistory: true
                    });
                }
            });
        }
    }

    /**
     * Verifica se o usuário pode utilizar a
     * digital para a geração de receitas.
     */
    public generateRecipe() {
        this.fingerprintAuth.available().then((result: BiometricIDAvailableResult) => {
            this.passwordToRecipe(result.any);
        });
    }

    /**
     * Informa as credenciais.
     */
    public passwordToRecipe(auto) {
        if(auto){
            this.useFingerPrint();
        } else {
            dialogs.prompt({
                title: "Geração da Receita",
                message: "Necessário para confirmar a autenticidade da receita.",
                okButtonText: "Verificar Senha",
                cancelButtonText: "Cancelar",
                inputType: dialogs.inputType.password
            }).then(r => {
                const login = {
                    email: this.userProfile.email,
                    password: r.text
                };
                this.confirmGenerateRecipe(login);
            });
        }
    }

    /**
     * Valida os dados de acesso para geração de receita.
     */
    public confirmGenerateRecipe(authentication) {
        this.clientService.postAll('api-login', authentication, true, ['error']).then((response: any) => {
            if(response.success) this.newRecipe();
        });
    }

    /**
     * Aciona a opção de login automático do dispositivo.
     */
    public useFingerPrint() {
        console.dir('useFingerPrint');
        this.fingerprintAuth.verifyFingerprint({
            title: "Biometria",
            message: "Verificando o acesso",
            authenticationValidityDuration: 10,
            useCustomAndroidUI: false
        }).then((enteredPassword?: string) => {
            if (!enteredPassword) {
                const login = JSON.parse(
                  ApplicationSettings.getString("possibleFingerprintAuth")
                );
                this.confirmGenerateRecipe(login);
            }
        })
        .catch(() => {
            this.notificationService.showFeedback({
            type: "error",
            message:
                "Biometria não reconhecida, informe os seus dados de acesso."
            });
            this.passwordToRecipe(false);
        });
    }

    /**
     * Realiza a possível interação medicamentosa.
     */
    public drugInteraction(activePrinciple) {
      this.interactionList.push(activePrinciple);
      let allInteration = {
        interaction: this.interactionList.toString()
      }
      this.clientService.postAll(this.routers.interaction_medical, allInteration, false, ['error']).then((response: any) => {
          if(!response.menssage){
            response.forEach(item => {
                this.notificationService.showFeedback({
                    type: "warning",
                    message: item.recommendation
                });
            });
          }
      });
    }
}
