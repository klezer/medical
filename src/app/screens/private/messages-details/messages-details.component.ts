import { ActivatedRoute } from '@angular/router';
import { Component, NgZone } from "@angular/core";
import { ClientService, PusherService, EventEmitterService } from "./../../../services";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import { Label } from "tns-core-modules/ui/label";
import { Page } from "tns-core-modules/ui/page";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { screen } from "tns-core-modules/platform";
import { topmost } from "tns-core-modules/ui/frame";

@Component({
    selector: "MessagesDetails",
    moduleId: module.id,
    templateUrl: "./messages-details.component.html",
    styleUrls: ['./messages-details.component.scss'],
    providers: [PusherService]
})

export class MessagesDetailsComponent {
    public delayTimer: any;
    public isAndroid = isAndroid;
    public isIOS = isIOS;
    public userProfile: any;
    public otherUserInfo: any = null;
    public urlParameter: any;
    public formComment: any;
    public submitted = false;
    public inProccess = false;
    public loadedAsync = false;
    public recipe: any;
    public recipeID: any;
    public patientID: any;
    public allComments: any = {
        list: [],
        history: true,
        loaded: false
    };
    public heightBox: any = 44.72;
    public screen = screen;
    public messages: any;
    public topmost = topmost;
    public writingComment = false;
    public routers = {
        getPatientInfo: 'doctor-recipe-info-patient',
        getMessages: 'doctor-all-comments-recipe',
        createMessage: 'doctor-comment-create'
    }

    constructor(private ActivatedRoute: ActivatedRoute,
        private clientService: ClientService,
        public page: Page,
        private pusherService: PusherService,
        private zone:NgZone){
        /** Remove o Action Bar padrão */            
        this.page.actionBarHidden = true;
        /** Resgata os dados do usuário logado */
        this.userProfile = JSON.parse(ApplicationSettings.getString('userProfile'));
         /** Recebe os parâmetros da rota */
         this.ActivatedRoute.params.forEach((params: any) => {
            this.recipeID = params.recipe_id;
            this.patientID = params.patient_id;
            ApplicationSettings.setString('roomMessagesActived', params.recipe_id);
        });
        this.userProfile = JSON.parse(ApplicationSettings.getString("userProfile"));
        this.formControl();
      }
    
      ngAfterContentInit(): void {
        this.loadedConfirm();
      }
    
      /**
       * confirmação de carregamento da API.
       */
      public loadedConfirm() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
          this.paginateMessages(1);
          this.clientService
            .getById(this.routers.getPatientInfo, this.recipeID)
            .then((response: any) => {
              this.otherUserInfo = response;
              this.otherUserInfo.recipe_id = this.recipeID;
            });
          this.loadedAsync = true;
        }, 450);
      }

    /**
     * Ajusta a altura do campo de acordo com o texto em no máximo 215.
     */
    public resizeHeightField(args) {
        let label = <Label>args.object;
        const maxHeigth = 215
        if (label.getActualSize().height < maxHeigth) {
            this.heightBox = label.getActualSize().height;
        } else {
            this.heightBox = maxHeigth;
            label.height = maxHeigth;
        }
    }

    /**
     * Ajusta a altura original do campo em 44.72.
     */
    public resetHeightField() {
        this.formComment.controls['comments'].setValue('');
        let label: any = this.page.getViewById('myText');
        if(label){
            label.text = '';
            label.height = 44.72;
            this.heightBox = 44.72;
        }
    }

    /**
     * Pagina todas as mensagens.
     */
    public paginateMessages(page) {
        this.allComments.loaded = false;
        let params = this.recipeID + '/' + this.userProfile.id + '?page=' + page;
        this.clientService.getById(this.routers.getMessages, params).then((response: any) => {
            this.allComments.history = (response.length > 0);
            if (response.length > 0) {
                page = page + 1;
                response.reverse().forEach((element, index) =>{
                    this.allComments.list.splice(0, 0, element);
                    this.allComments.loaded = (response.length -1 == index);
                });
            }
        });
    }

    /**
     * Define todo os campos do formuário 
     * com suas regras de validação.
     */
    private formControl() {
        /** Formulário de Comentário */
        this.formComment = new FormGroup({
            user_name: new FormControl(this.userProfile.name),
            time: new FormControl((new Date).getTime() / 1000),
            user_id: new FormControl(this.userProfile.id, [Validators.required]),
            recipes_id: new FormControl(this.recipeID, [Validators.required]),
            comments: new FormControl('', [Validators.required, Validators.minLength(2)])
        });
    }

    /**
     * Insere o novo comentário a lista.
     */
    public insertNewComment(new_comment) {
        this.allComments.list.splice(this.allComments.list.length, 0, new_comment);
        this.resetHeightField();
        this.finishWrite();
        this.inProccess = false;
    }

    /**
     * Realiza a criação dos comentários.
     */
    public sendComment() {
        this.submitted = true;
         if(this.formComment.valid){
            this.inProccess = true;
            let frozenMessage = JSON.stringify(this.formComment.value);
            let newMessage = JSON.parse(frozenMessage);
            this.insertNewComment(this.formComment.value);
            this.clientService.postAll(this.routers.createMessage, newMessage, true, ['error']);
        }
    }

    /**
     * Realiza conexão do pusher.
     */
    public connectPusher() {
        this.pusherService.connect();
    }

    /**
     * Realiza a desconexão do pusher.
     */
    public disconnectPusher() {
        ApplicationSettings.remove('roomMessagesActived');
        this.pusherService.disconnect();
    }

    /**
     * Monitora as mensagens.
     */
    public subscribe() {
        this.pusherService.pusher.subscribeToChannelEvent(`recipe-${this.recipeID}`, 'message', (error, data) => {
            if (!error) {
                this.zone.run(() => {
                    if (this.userProfile.name != data['data'].user_name) {
                        this.insertNewComment(data['data']);
                        EventEmitterService.get("newMessage").emit('true');
                    } else {
                        let lastComment = this.allComments.list[this.allComments.list.length-1];
                        lastComment.checked = lastComment.comments == data['data'].comments;
                    }
                });
            }
        });
    }

    /**
     * Evento para alertar o início da digitação.
     */
    public startWrite() {
        this.writingComment = true;
    }

    /**
     * Evento para alertar o fim da digitação.
     */
    public finishWrite() {
        this.writingComment = false;
    }
}
