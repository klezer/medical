import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MessagesDetailsComponent } from "./messages-details.component";

const routes: Routes = [
    { path: "", component: MessagesDetailsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MessagesDetailsRoutingModule { }
