import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AnamnesisComponent } from "./anamnesis.component";

const routes: Routes = [
    { path: "", component: AnamnesisComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AnamnesisRoutingModule { }
