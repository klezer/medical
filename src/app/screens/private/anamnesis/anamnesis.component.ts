import { Component } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { ClientService } from "./../../../services";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

@Component({
    selector: "anamnesis",
    moduleId: module.id,
    templateUrl: "./anamnesis.component.html",
    styleUrls: ['./anamnesis.component.scss']
})

export class AnamnesisComponent {
   public delayTimer: any;
    public userProfile: any;
    public loadedAsync: boolean = false;
    public loadedView: boolean = false;
    public patientID: any;
    public routers = {
        getAnamnesePatient: 'doctor-get-anamnese-patient'
    }
    public anamneseInfo: any = [];

    constructor(private ActivatedRoute: ActivatedRoute,
                private clientService: ClientService,
                public page: Page){
        /** Remove o Action Bar padrão */            
        this.page.actionBarHidden = true;
        /** Resgata os dados do usuário logado */
        this.userProfile = JSON.parse(ApplicationSettings.getString('userProfile'));
         /** Recebe os parâmetros da rota */
         this.ActivatedRoute.params.forEach((params) => { 
            this.patientID = params.patient_id;
         });
    }

    ngOnInit(): void {
      this.onDrawerButtonTap(false);
    }

    ngAfterContentInit(): void {
      this.loadedConfirm();
    }

    /** 
     * Abre/fecha menu. 
     */
    public onDrawerButtonTap(confirm): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        if (sideDrawer !== undefined && sideDrawer.getIsOpen()){
            sideDrawer.closeDrawer();
        } if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm){
            sideDrawer.showDrawer();
        }
    }

        /**
     * confirmação de carregamento da API.
     */
    public loadedConfirm() {
      clearTimeout(this.delayTimer);
      this.delayTimer = setTimeout(() => {
         this.getStructure();
         this.loadedAsync = true;
        }, 450);
   }

   /**
    * Realiza a montagem da estrutura de perguntas.
    */
   public getStructure() {
      this.clientService.getById(this.routers.getAnamnesePatient, this.patientID).then((response: any) => {
         this.anamneseInfo = response;
         this.loadedAsync = true;
     });
   }

}