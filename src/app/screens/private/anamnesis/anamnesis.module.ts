import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { AnamnesisRoutingModule } from "./anamnesis-routing.module";
import { AnamnesisComponent } from "./anamnesis.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        AnamnesisRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        AnamnesisComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        AnamnesisComponent
    ]
})
export class AnamnesisModule { }
