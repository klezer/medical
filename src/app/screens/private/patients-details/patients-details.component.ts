import { Component } from "@angular/core";
import { ClientService } from "./../../../services";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import 'moment/min/locales';
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from '@angular/router';
import { screen } from "tns-core-modules/platform";
    
@Component({
    selector: "PatientsDetails",
    moduleId: module.id,
    templateUrl: "./patients-details.component.html",
    styleUrls: ['./patients-details.component.scss']
})

export class PatientsDetailsComponent {
    public delayTimer: any;
    public profile: any;
    public isAndroid = isAndroid;
    public isIOS = isIOS;
    public screen: any;
    public searchQuery: string;
    public patients: any = null;
    public options: any;
    public now: any;
    public loadedAsync: boolean = false;
    public userProfile: any;
    public listKeys: any = [];
    public counter = 0;
    public menuList;
    public patientID: any;
    public patientInfo: any;
    public allRecipes: any = [];
    public routers = {
        retrieveAll: 'doctor-list-all-recipe-for-patient',
        getPatientInfo: 'doctor-info-cad-patient'
    }
    
    constructor(private clientService: ClientService,
                public page: Page,
                private router: RouterExtensions,
                private ActivatedRoute: ActivatedRoute,){
        /** Remove o Action Bar padrão */            
        page.actionBarHidden = true;
        /** Recebe os parâmetros da rota */
        this.ActivatedRoute.params.forEach((params) => { 
            this.patientID = params.patient_id;
        });
        this.userProfile = JSON.parse(ApplicationSettings.getString('userProfile'));
        /** Pega todos os dados do paciente */
        this.clientService.getById(this.routers.getPatientInfo, this.patientID).then((response: any) => {
            this.patientInfo = {
                name: response.personal.name,
                image: response.picture.image,
                telephone: response.contact.telephone_one,
                email: response.personal.email
            }
        });
        /** Definições da tela */
        this.screen = screen;
    }

    ngAfterContentInit(): void {
        this.loadedConfirm();
    }

    /**
     * confirmação de carregamento da API.
     */
    public loadedConfirm() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.getAllRecipesForPatient();
        }, 450);
    }

    /**
     * Pega todas as receitas do paciente.
     */
    public getAllRecipesForPatient() {
        this.clientService.getAll(this.routers.retrieveAll, `${this.patientID}/${this.userProfile.id}`, ['error']).then((response: any) => {
            this.allRecipes = response;
            this.loadedAsync = true;
        });
    }
}
