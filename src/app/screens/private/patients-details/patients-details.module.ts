import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { PatientsDetailsRoutingModule } from "./patients-details-routing.module";
import { PatientsDetailsComponent } from "./patients-details.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
/** Diretivas */
import { ContactUserModule } from './../../../directives/contact-user/contact-user.module';
import { RecipeItemModule } from './../../../directives/recipe-item/recipe-item.module';

@NgModule({
    imports: [
        ContactUserModule,
        RecipeItemModule,
        NativeScriptCommonModule,
        PatientsDetailsRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        PatientsDetailsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        PatientsDetailsComponent
    ]
})
export class PatientsDetailsModule { }
