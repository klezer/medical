import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { PatientsDetailsComponent } from "./patients-details.component";

const routes: Routes = [
    { path: "", component: PatientsDetailsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PatientsDetailsRoutingModule { }
