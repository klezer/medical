import { Component, NgZone} from "@angular/core";
import { ClientService, EventEmitterService } from "./../../../services";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import 'moment/min/locales';
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Page } from "tns-core-modules/ui/page";
import { screen } from "tns-core-modules/platform";

@Component({
    selector: "Messages",
    moduleId: module.id,
    templateUrl: "./messages.component.html",
    styleUrls: ['./messages.component.scss']
})

export class MessagesComponent {
    public delayTimer: any;
    public isAndroid = isAndroid;
    public isIOS = isIOS;
    public screen = screen;
    public messages = [];
    public loadedAsync = false;
    public userProfile: any;
    public routers = {
        retrieveAll: 'doctor-list-message'
    };
    public currentPage = 1;
    public displayOption = {
        emptyList: true,
        hasList: false
    }
    constructor(private clientService: ClientService,
                public page: Page,
                private zone: NgZone){
        this.monitorPushMessages();  
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.onDrawerButtonTap(false);
        this.getUserInfo();
        ApplicationSettings.remove('roomMessagesActived');
    }

    ngAfterContentInit(): void {
        this.loadedConfirm();
    }

    /**
     * confirmação de carregamento da API.
     */
    public loadedConfirm() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.getAll(false);
          }, 450);
    }

    /**
     * Verifica e pega as informações do usuário recém logado.
     */
    public getUserInfo() {
        if (ApplicationSettings.hasKey("userProfile")) {
            this.userProfile = JSON.parse(
                ApplicationSettings.getString("userProfile")
            );
        }
    }

    /**
     * Pega os itens.
     */
    public getAll(search) {
        this.clientService
        .getAllPaginate(
            this.routers.retrieveAll,
            this.userProfile.id,
            this.currentPage,
            ["error"],
            search
        )
        .then((response: any) => {
            this.loadedAsync = true;
            if (response.length > 0) {
                this.changeScreen('hasList');
                this.currentPage = this.currentPage + 1;
                this.messages = response;
            }
        });
    }

    /**
     * Pagina mais itens.
     */
    public loadMore() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.getAll(false);
        }, 1000);
    }

    /**
     * Realiza a consulta do termo bucado.
     */
    public search(searchInfo) {
        this.loadedAsync = false;
        this.currentPage = 1;
        this.messages = [];
        this.getAll(searchInfo.length > 0 ? searchInfo : false);
    }

    /**
     * Monitora o Push Notification.
     */
    public monitorPushMessages() {
        EventEmitterService.get("event-chat").subscribe({
            next: (message: any) => {
                if(message.recipe_id && message.body) {
                    const indexMessage = this.messages.map(item => item.recipe_id).indexOf(parseInt(message.recipe_id));
                    this.updateMessages(indexMessage, message);
                }
            }
        });
    }

    /**
     * Atualiza os dados das mensagem.
     */
    public updateMessages(index, message) {
        this.zone.run(() => {
            this.messages[index].comments = message.body;
            this.messages[index].last_comments = parseInt(this.messages[index].last_comments) + 1;
        });
    }

    /** Abre/fecha menu  */
    public onDrawerButtonTap(confirm): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        if (sideDrawer && sideDrawer.getIsOpen()){
            sideDrawer.closeDrawer();
        } if (sideDrawer && !sideDrawer.getIsOpen() && confirm){
            sideDrawer.showDrawer();
        }
    }
  
  /**
   * Altera a visualização desejada.
   */
  public changeScreen(screen) {
    Object.keys(this.displayOption).forEach(view => {
      this.displayOption[view] = false;
    });
    this.displayOption[screen] = true;
  }
}
