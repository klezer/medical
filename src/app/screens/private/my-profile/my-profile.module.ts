import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MyProfileRoutingModule } from "./my-profile-routing.module";
import { MyProfileComponent } from "./my-profile.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { DropDownModule } from "nativescript-drop-down/angular";
import { ReactiveFormsModule } from '@angular/forms';
import { MaskedTextFieldModule } from "nativescript-masked-text-field/angular";
@NgModule({
    imports: [
        NativeScriptCommonModule,
        MyProfileRoutingModule,
        NativeScriptFormsModule,
        DropDownModule,
        ReactiveFormsModule,
        MaskedTextFieldModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        MyProfileComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MyProfileModule { }
