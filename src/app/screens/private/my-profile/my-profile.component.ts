import { Component } from "@angular/core";
import { ClientService, NotificationService, UtilsService } from "./../../../services";
import { Page } from "tns-core-modules/ui/page";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValueList, SelectedIndexChangedEventData } from "nativescript-drop-down";
import { TextField } from "tns-core-modules/ui/text-field";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import * as moment from 'moment'; 
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Switch } from "tns-core-modules/ui/switch";
import { GoogleAutoCompleteService } from '../../../services/google-auto-complete/google-auto-complete.service';

@Component({
    selector: 'MyProfile',
    moduleId: module.id,
    templateUrl: "./my-profile.component.html",
    styleUrls: ["./my-profile.component.scss"]
})

export class MyProfileComponent {
    public delayTimer: any;
    public isAndroid = isAndroid;
    public isIOS = isIOS;
    public userProfile: any;
    public formMyProfile: any;
    public myProfile: any;
    public submitted = false;
    public genres: ValueList<string>;
    public FormGenres: any;
    public specialties: ValueList<string>;
    public FormSpecialties: any;
    public advices: ValueList<string>;
    public FormAdvices: any;
    public states: ValueList<string>;
    public FormStates: any;
    public citys: ValueList<string>;
    public FormCitys: any;
    public Address: any;
    public select_state_value = 0;
    public select_city_value = 0;
    public select_genre_value = 0;
    public select_region_value = 0;
    public select_speciality_value: number = 0;
    public select_advice_value: number = 0;
    public loadedAsync = false;
    public loadedLists = false;
    public changePassword = false;
    public moment = moment;
    public rule = /[^a-z0-9]/gi;
    public routers = {
        updateAll: 'doctor-cad-update',
        retrieveAll: 'doctor-cad-get',
        listStates: 'doctor-cad-states',
        listCitys: 'doctor-cad-citys',
        listAdvices: 'doctor-list-advice',
        listSpecialties: 'doctor-list-specialty'
    }
    public updateImage = false;

    constructor(private clientService: ClientService,
                private googleAutoCompleteService: GoogleAutoCompleteService,
                private notificationService: NotificationService,
                public utilsService: UtilsService,
                public page: Page){         
        this.page.actionBarHidden = true;
         this.userProfile = JSON.parse(ApplicationSettings.getString('userProfile'));
        /** Cidades */  
        this.FormCitys = [
            { value: "", display: "Cidade"}
        ];
        this.citys =  new ValueList<string>(this.FormCitys);
        /** Estados */  
        this.FormStates = [
            { value: "", display: "UF"}
        ];
        this.states =  new ValueList<string>(this.FormStates);
        /** Gêneros */  
        this.FormGenres = [
            { value: "", display: "Gênero"},
            { value: "F", display: "Feminino" },
            { value: "M", display: "Masculino" }
        ];
        this.genres =  new ValueList<string>(this.FormGenres);
        /** Atualiza os estados */
        this.clientService.getAll(this.routers.listStates, null, ['error']).then(response => {
            this.FormStates = response;
            this.FormStates.splice(0, 0, { value: "", display: "UF"});
            this.states =  new ValueList<string>(this.FormStates);
        });   
        /** Especialidade */  
        this.FormSpecialties = [
            { value: "", display: "Especialidade"}
        ];
        this.specialties =  new ValueList<string>(this.FormSpecialties);
        // Atualiza os conselhos
        this.clientService.getAll(this.routers.listAdvices, null, ['error']).then(response => {
            this.FormAdvices = response;
            this.FormAdvices.splice(0, 0, { value: "", display: "Conselho"});
            this.advices =  new ValueList<string>(this.FormAdvices);
            /** Conclui a atualização das listas */
            this.loadedLists = true;
        });
        
    }

    ngOnInit(): void {
        this.onDrawerButtonTap(false);  
    }

    ngAfterContentInit(): void {
        this.formControl(null);
        this.loadedConfirm();
    }

    /**
     * confirmação de carregamento da API.
     */
    public loadedConfirm() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.getMyProfileForEdit();
        }, 450);
    }

    /**
    * Pega todos os meus dados do perfil.
    */
   public getMyProfileForEdit() {
        this.clientService.getAll(this.routers.retrieveAll, this.userProfile.id, ['error']).then((response: any) => {
            this.myProfile = response;
            this.updateLastImage();
            this.formControl(this.myProfile);
            this.resolveSelectField(
                this.myProfile.personal.genre, 
                this.myProfile.personal.advice, 
                this.myProfile.personal.region_id, 
                this.myProfile.personal.specialty, 
                this.myProfile.address.state_id, 
                this.myProfile.address.city_id
            );
        });
    }

    /**
     * Atualiza a última imagem
     */
    public updateLastImage() {
        if (this.userProfile.image !== this.myProfile.picture.image) {
        this.updateImage = true;
        this.userProfile.image = this.myProfile.picture.image;
        ApplicationSettings.setString(
            "userProfile",
            JSON.stringify(this.userProfile)
        );
        }
    }
    /**
     * Define todo os campos do formuário 
     * com suas regras de validação.
     */
    private formControl(myProfile) {
        let form = {
            user_id: new FormControl(this.userProfile.id),
            name: new FormControl((myProfile != null) ? myProfile.personal.name : '', [Validators.required]),
            advice_id: new FormControl('', [Validators.required]),
            region_id: new FormControl('', [Validators.required]),
            specialty_id: new FormControl('', [Validators.required]),
            crm_number: new FormControl((myProfile != null) ? myProfile.personal.crm_number : '', [Validators.required]),
            email: new FormControl((myProfile != null) ? myProfile.personal.email : '', [Validators.required, Validators.email]),
            telephone_one: new FormControl((myProfile != null) ? myProfile.contact.telephone_one : '', [Validators.required]),
            cpf: new FormControl((myProfile != null) ? myProfile.personal.cpf : '', [Validators.required]),
            rg: new FormControl((myProfile != null) ? myProfile.personal.rg : '', [Validators.required]),
            address_id: new FormControl((myProfile != null) ? myProfile.address.address_id : '', [Validators.required]),
            locality: new FormControl((myProfile != null) ? myProfile.address.locality : '', [Validators.required]),
            street_code: new FormControl((myProfile != null) ? myProfile.address.street_code : '', [Validators.required]),
            street_title: new FormControl((myProfile != null) ? myProfile.address.street_title : '', [Validators.required]),
            number: new FormControl((myProfile != null) ? myProfile.address.number : '', [Validators.required]),
            complement: new FormControl((myProfile != null) ? myProfile.address.complement : ''),
            district_title: new FormControl((myProfile != null) ? myProfile.address.district_title : '', [Validators.required]),
            genre: new FormControl('', [Validators.required]),
            document_professional: new FormControl('upload', [Validators.required]),
            date_of_birth: new FormControl((myProfile != null) ? myProfile.personal.date_of_birth : '', [Validators.required]),
            city_id: new FormControl('', [Validators.required]),
            state_id: new FormControl('', [Validators.required]),
            password: new FormControl(''),
            password_confirmation: new FormControl(''),
        };
        // Se desejar renovar a senha
        if (this.changePassword) {
            form.password = new FormControl('', [Validators.required, Validators.minLength(6)]);
            form.password_confirmation = new FormControl('', [Validators.required, Validators.minLength(6)]);
        }
        this.formMyProfile = new FormGroup(form);
    }

    /**
     * Se desejar altera a senha.
     */
    public onChecked(args) {
        this.changePassword = (<Switch>args.object).checked;
        this.formControl(this.myProfile);
        this.resolveSelectField(this.myProfile.personal.genre, this.myProfile.personal.advice, this.myProfile.personal.region, this.myProfile.personal.specialty, this.myProfile.address.state_id, this.myProfile.address.city_id);
    }

    /**
     * Define os valores corretos nos selects.
     */
    public resolveSelectField(genre, advice, region, speciality, state, city) {
        // Definição de sexo
        let newGenre = (/[^a-z]/gi.test(genre)) ? genre == 1 ? 'F': 'M' : genre;
        this.select_genre_value = parseInt(this.FormGenres.map((item) => item.value).indexOf(newGenre.toUpperCase()));
        this.formMyProfile.controls.genre.setValue(this.select_genre_value);
        // Definição de conselho
        this.select_advice_value = parseInt(this.FormAdvices.map((item) => item.value).indexOf(advice));
        this.formMyProfile.controls.advice_id.setValue(this.select_advice_value);     
        // Definição de conselho
        this.select_region_value = parseInt(this.FormStates.map((item) => item.value).indexOf(region));
        this.formMyProfile.controls.region_id.setValue(this.select_region_value);
        // // Definição de especialidade
        this.autoCompleteSpecialty(this.select_advice_value).then(() => {
            this.select_speciality_value = parseInt(this.FormSpecialties.map((item) => item.value).indexOf(speciality));
            this.formMyProfile.controls.specialty_id.setValue(this.select_speciality_value);  
        });
        // Definição de estado
        this.select_state_value = parseInt(this.FormStates.map((item) => item.value).indexOf(state));
        this.formMyProfile.controls.state_id.setValue(this.select_state_value);     
        // Definição de cidade
        this.autoCompleteCity(this.select_state_value).then(() => {
            this.select_city_value = parseInt(this.FormCitys.map((item) => item.value).indexOf(city));
            this.formMyProfile.controls.city_id.setValue(this.select_city_value);  
        });
        this.loadedAsync = true;
   }

    /**
     * Realiza o cadastro do novo usuário.
     */
    public MyProfileUser() {
        this.submitted = true;
        let formatData = this.formatData(this.formMyProfile.value)
        if(this.changePassword){
            if(this.formMyProfile.get('password').value != this.formMyProfile.get('password_confirmation').value) {
                this.notificationService.showFeedback({
                    type: "warning",
                    message: 'As senhas não coincidem.'
                  });
                return false;
            }
        }
        if (formatData){
            this.submitProfile(formatData); 
        }
              
    }

    /**
     * Submete os dados já validados para a API.
     */
    public submitProfile(formatData) {
        this.utilsService.dismissSoftKeybaord();
        this.submitted = true;
        this.clientService.postAll(this.routers.updateAll, formatData, false, ['error']).then((response: any) => {
            // próximo passo do cadastro
            this.submitted = false;
            if(response.success){
                this.notificationService.showFeedback({
                    type: "success",
                    message: response.success
                  });
            }
        })
    }

    /**
     * Pega o endereco API do Google Maps.
     */
    public getAddressByCep(args) {
        let onlyNumbers = (<TextField>args.object).text.replace(/[^0-9]/g,'');
        // Verifica se é númerico e possui a quantidade necessária para pesquisa
        if(/\d/.test(onlyNumbers) && onlyNumbers.length >= 7){
            this.googleAutoCompleteService.getAddressByInfo(onlyNumbers, true).then((response) => {
                const address = this.googleAutoCompleteService.filterAddress(response);
                this.autoCompleteAddressFiels(address.street_title, address.district_title, address.mycity, address.state);
            });
         }
    }

    /**
     * Insere os dados nos campos de endeço.
     */
    public autoCompleteAddressFiels(street_title, district_title, mycity, state) {
        // define a rua
        if(street_title){
            this.formMyProfile.controls['street_title'].setValue(street_title);
        }
        // define o bairro
        if(district_title){
            this.formMyProfile.controls['district_title'].setValue(district_title);
        }
        // define o estado
        this.select_state_value = this.FormStates.map(item => item.display).indexOf(state);
        this.formMyProfile.controls['state_id'].setValue(this.FormStates[this.select_state_value].value);
        this.autoCompleteCity(this.select_state_value).then(() => {
            this.select_city_value = this.FormCitys.map(item => item.display).indexOf(mycity);
            this.formMyProfile.controls['city_id'].setValue(this.FormCitys[this.select_city_value].value);
        });
    }
          
    /**
     * Observa as mudanças do componete de selecão e atribui os valores.
     */
    public changeSelect(args: SelectedIndexChangedEventData, field) {
        // Para estado
        if(field === 'state_id'){
            this.autoCompleteCity(args.newIndex);
            this.formMyProfile.controls[field].setValue(this.FormStates[args.newIndex].value);
            this.select_state_value = args.newIndex;
        }
        // Para Conselho
        else if( field === 'advice_id'){
            this.autoCompleteSpecialty(args.newIndex);
            this.formMyProfile.controls[field].setValue(this.FormAdvices[args.newIndex].value);
            this.select_advice_value = args.newIndex;
        }
        // Para Conselho
        else if( field === 'region_id'){
            this.formMyProfile.controls[field].setValue(this.FormStates[args.newIndex].value);
            this.select_region_value = args.newIndex;
        }
        // Para gênero
        else if( field === 'genre'){
            this.formMyProfile.controls[field].setValue(this.FormGenres[args.newIndex].value);
        }
        // Para cidade
        else if( field === 'city_id'){
            this.formMyProfile.controls[field].setValue(this.FormCitys[args.newIndex].value);
            this.select_city_value = args.newIndex;
        }
        // Para especialidade
        else if( field === 'specialty_id'){
            this.formMyProfile.controls[field].setValue(this.FormSpecialties[args.newIndex].value);
        }
    }
    
    /**
     * Realiza o autocomplete do do estado/cidade.
     */
    public autoCompleteCity(state) {
        return new Promise((resolve) => {
            this.clientService.getById(this.routers.listCitys, this.FormStates[state].value).then((response: any) => {
                this.FormCitys = response.citys;
                this.FormCitys.splice(0, 0, { value: '', display: 'Selecione'});
                this.citys = new ValueList<string>(this.FormCitys);
                resolve(state);
            });
        });
    }
    
    /**
     * Realiza o autocomplete da especialidade.
     */
    public autoCompleteSpecialty(advice) {
        return new Promise((resolve) => {
            this.clientService.getById(this.routers.listSpecialties, this.FormAdvices[advice].value).then((response: any) => {
                this.FormSpecialties = response;
                this.FormSpecialties.splice(0, 0, { value: '', display: 'Selecione'});
                this.specialties = new ValueList<string>(this.FormSpecialties);
                resolve(advice);
            });
        });
    }

    /**
     * Define valores padrões para o campo de cidade.
     */
    public changeSelectClosed() {
        this.select_city_value = 0;
        this.formMyProfile.controls['city_id'].setValue();
    }

    /**
     * Formata os dados do formulário.
     */
    public formatData(value) {
        // Remove as máscaras
        let rule = /[^a-z0-9]/gi;
        let form = JSON.stringify(value);
        let newForm = JSON.parse(form);
        let formRegister = newForm;
        // Valida a data
        if(!this.formMyProfile.get('date_of_birth').invalid && this.formMyProfile.get('date_of_birth') != null){
            let date = newForm['date_of_birth'].split('/');
            let year = date[2].replace(rule, '');
            let month = date[1].replace(rule, '');
            let day = date[0].replace(rule, '');
            if (year.length == 4 && (month.length && day.length)  == 2){
                formRegister.date_of_birth = year + '-' + month + '-'+ day;
            } else {
                this.notificationService.showFeedback({
                    type: "warning",
                    message: 'Data de nascimento inválido.'
                  });
                return false;
            }
        }
        // Valida outros campos
        formRegister.telephone_one = (newForm['telephone_one'] != null) ? newForm['telephone_one'].replace(rule, '') : '';
        formRegister.cpf = (newForm['cpf'] != null) ? newForm['cpf'].replace(rule, '') : '';
        formRegister.street_code = (newForm['street_code'] != null) ? newForm['street_code'].replace(rule, '') : '';
        // Retorna os valores tratados
        return formRegister;
    }

     /** Abre/fecha menu  */
     public onDrawerButtonTap(confirm): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        if (sideDrawer && sideDrawer.getIsOpen()){
            sideDrawer.closeDrawer();
        } if (sideDrawer && !sideDrawer.getIsOpen() && confirm){
            sideDrawer.showDrawer();
        }
    }

}
