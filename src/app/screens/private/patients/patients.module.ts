import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { PatientsRoutingModule } from "./patients-routing.module";
import { PatientsComponent } from "./patients.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
/** Diretivas */
import { SimpleItemModule } from './../../../directives/simple-item/simple-item.module';
import { SearchTopBarModule } from './../../../directives/search-top-bar/search-top-bar.module';
@NgModule({
    imports: [
        SimpleItemModule,
        SearchTopBarModule,
        NativeScriptCommonModule,
        PatientsRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        PatientsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        PatientsComponent
    ]
})
export class PatientsModule { }
