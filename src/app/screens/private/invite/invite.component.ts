import { Component } from "@angular/core";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as appavailability from "nativescript-appavailability";
import { openUrl } from "tns-core-modules/utils/utils";
import * as email from "nativescript-email";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TextField } from "tns-core-modules/ui/text-field";
import { screen } from 'tns-core-modules/platform';
import { ClientService } from './../../../services';

@Component({
    selector: "invite",
    moduleId: module.id,
    templateUrl: "./invite.component.html",
    styleUrls: ['./invite.component.scss']
})

export class InviteComponent {
  public screen = screen;
  public userProfile: any;
  public hasOption = {
    email: false,
    whatsapp: false
  };
  public displayOption = {
    sendToEmail: false,
    sendToWhatsApp: false
  };
  private routers = {
    storeInvite: 'store-doctor-invitation'
  };
  public form: FormGroup;
  public rule = /[^a-z0-9]/gi;
  public validatePhone = false;

    constructor(public page: Page,
                private clientService: ClientService){         
      this.page.actionBarHidden = true;
      this.userProfile = JSON.parse(ApplicationSettings.getString('userProfile'));
      this.formControl('email');
    }

    ngOnInit(): void {
      this.onDrawerButtonTap(false);
    }

    /**
     * Define todo os campos do formuário 
     * com suas regras de validação.
     */
    private formControl(type) {
      let newForm = {
          phone: new FormControl(''),
          email: new FormControl('')
      };

      if (type == 'whatsapp') {
        this.validatePhone = false;
        newForm.phone =  new FormControl('', [Validators.required]);
      } else {
        newForm.email =  new FormControl('', [Validators.required, Validators.email]);
      }

      this.form = new FormGroup(newForm);
    }

    /**
     * Recebe o valor do campo e insere no formulário.
     */
    public getValue(args, field) {
      let value = (<TextField>args.object).text;
      let valueFormated = value ? value.replace(this.rule, '') : '';
      this.form.controls[field].setValue(valueFormated);
      this.validatePhone = (valueFormated.length > 9);
    }

    /** 
     * Abre/fecha menu. 
     */
    public onDrawerButtonTap(confirm): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        if (sideDrawer !== undefined && sideDrawer.getIsOpen()){
            sideDrawer.closeDrawer();
        } if (sideDrawer !== undefined && !sideDrawer.getIsOpen() && confirm){
            sideDrawer.showDrawer();
        }
    }

    /**
     * Altera a visualização desejada.
     */
    public changeScreen(screen) {
      Object.keys(this.displayOption).forEach(view => {
        this.displayOption[view] = false;
      });
      this.displayOption[screen] = true;
    }

    /**
     * Verifica os possíveis tipos de envio.
     * E-mail/WhatsApp
     */
    public verifyOptions() {
      appavailability.available("com.whatsapp").then((avail: boolean) => {
        this.hasOption.whatsapp = avail;
        if(!avail) {
          appavailability.available("whatsapp://").then((avail: boolean) => {
            this.hasOption.whatsapp = avail;
          });
        }
      });
      email.available().then((avail: boolean) => {
        this.hasOption.email = avail;
        if(avail) this.changeScreen('sendToEmail');
      });
    }

    /**
     * Envia por WhatsApp.
     */
    public sendToWhatsApp() {
      let message = encodeURI('Dr Pediu - Convite | Baixe o Dr. Pediu em https://drpediu.doc88.com.br');
      openUrl(`https://api.whatsapp.com/send?phone=+55${ this.form.controls['phone'].value }&text=${message}`);
      this.registerInvite(this.form.controls['phone'].value, 'whattsapp');
    }

    /**
     * Envia por e-mail.
     */
    public sendEmail() {
      email.compose({
        subject: "Dr Pediu - Convite",
        body: `Baixe o Dr. Pediu em https://drpediu.doc88.com.br`,
        to: [this.form.controls['email'].value]
      });
      this.registerInvite(this.form.controls['email'].value, 'email');
    }

    /**
     * Realiza o registro do convite.
     * @param channel - Canal de whatsapp ou telefone
     * @param value - Telefone ou E-mail
     */
    private registerInvite(channel, value) {
      const data = {
        user_id: this.userProfile.id,
        value,
        channel,
        type_user: 'doctor'
      };
      this.clientService.postAll(this.routers.storeInvite, data, false, ['error', 'success']);
    }

}