import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { InviteRoutingModule } from "./invite-routing.module";
import { InviteComponent } from "./invite.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        InviteRoutingModule,
        NativeScriptFormsModule,
        ReactiveFormsModule,
        TNSFontIconModule.forRoot({
			"icomoon": "./fonts/icon-font-icomoon.css"
		})
    ],
    declarations: [
        InviteComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        InviteComponent
    ]
})
export class InviteModule { }
