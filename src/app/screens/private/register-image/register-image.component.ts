import { Component, AfterContentInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ClientService, FirebaseService, NotificationService, UtilsService } from "./../../../services";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { ImageAsset } from 'tns-core-modules/image-asset';
import { ImageSource } from "tns-core-modules/image-source";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import  * as FileSystem from "tns-core-modules/file-system";
import * as camera from "nativescript-camera";
import { ImageCropper } from "nativescript-imagecropper";
import {
  hasCameraPermissions,
  requestCameraPermissions
} from "nativescript-advanced-permissions/camera";
import * as permissionsCore from "nativescript-advanced-permissions/core";

@Component({
    selector: "RegisterImage",
    moduleId: module.id,
    templateUrl: "./register-image.component.html",
    styleUrls: ['./register-image.component.scss'],
    providers: [FirebaseService]
})

export class RegisterImageComponent implements AfterContentInit {
  public delayTimer: any; 
  public isAndroid = isAndroid;
  public isIOS = isIOS;
  public cameraImage: any;
  public userID: any;
  public editUserID: any = false;
  public valid: boolean = false;
  public options: any = { 
      width: 220, 
      height: 220, 
      keepAspectRatio: true,
      allowsEditing: true, 
      saveToGallery: true,
      cameraFacing: 'front' 
  };
  public imageTaken: ImageAsset;
  private imageCropper: ImageCropper;
  public finalImage: any;
  public routers = {
      registerImage: "doctor-profile-create",
      updateImage: "doctor-profile-update",
      login: 'api-login'
  };
  public inProccess = false;
  private filename: string;
  private path: string;
  constructor(private clientService: ClientService,
              private firebaseService: FirebaseService,
              private notificationService: NotificationService,
              private ActivatedRoute: ActivatedRoute,
              private utilsService: UtilsService,
              public page: Page){        
      page.actionBarHidden = true;
      this.ActivatedRoute.params.forEach((params: any) => {
          this.editUserID = params.user_id;
      });
  }

  ngAfterContentInit(): void {
      this.imageCropper = new ImageCropper();
      this.userID = ApplicationSettings.getNumber('userID');
      ApplicationSettings.setBoolean("registrationSteps", true);
  }

  /**
   * Adiciona a foto do usuário
   */
  public takePicture() {
    if (isAndroid) {
      camera.requestPermissions();
    } else {
      this.verifyPermission();
    }
    // Ativa a câmera
    camera.takePicture(this.options).
    then((imageAsset) => {
        let folder = FileSystem.knownFolders.documents();
        this.filename = new Date().getTime() + '.png';
        this.path = FileSystem.path.join(folder.path, this.filename);
        // Salvamento da imagem
        const source = new ImageSource();

        source.fromAsset(imageAsset).then((result) => {
            if(isAndroid){
                this.imageCropper.show(result, { width:220, height:220 }).then((args) => {
                    this.getImageOrigin(args.image, this.path);
                });        
            } else {
                this.getImageOrigin(result, this.path);
            }          
        });

    });
  }

  /**
   * Retorna diversas as opções de utilização da imagem.
   * @param result 
   * @param path 
   */
  public getImageOrigin (result, path) {
      let saved = result.saveToFile(path, 'png');
      if (saved) {
          let base64 = result.toBase64String("png", 80);
          this.cameraImage = 'data:image/png;base64,' + base64;
          this.finalImage = {
              user_id: this.editUserID ? this.editUserID : this.userID,
              image: "data:image/png;base64," + base64
          };
          clearTimeout(this.delayTimer);
          this.delayTimer = setTimeout(() => {
              this.valid = true;
          }, 600);
      }
  }

  /**
   * Realiza o login automático.
   */
  public uploadImage() {
    this.firebaseService.getCurrentPushToken().then((response: any) => {
      let finalRoute = this.editUserID
        ? this.routers.updateImage
        : this.routers.registerImage;
      this.inProccess = true;
      this.clientService
        .uploadFile(finalRoute, this.finalImage, this.filename, this.path)
        .then((response: any) => {
          if (response.responseCode !== 200) {
            this.notificationService.showFeedback({
              type: "error",
              message: "Erro! Tente enviar sua imagem novamente..."
            });
            this.inProccess = false;
          } else {
            if (this.editUserID) {
              this.notificationService.showFeedback({
                type: "success",
                message: "Atualização realizada com sucesso!"
              });
              this.utilsService.goToScreen("/my-profile");
            } else {
              this.makeLogin();
            }
          }
        });
    });
  }

  /**
   * Realiza o login após o cadastro da imagem.
   */
  public makeLogin() {
    const firstAccess = {
      email: ApplicationSettings.getString("firstAccess_email"),
      password: ApplicationSettings.getString("firstAccess_password"),
      device_token: ApplicationSettings.getString("deviceToken")
    };
    this.clientService
      .postAll(this.routers.login, firstAccess, true, ["error"])
      .then((response: any) => {
        if (response.success) {
          this.clientService.setApiServiceTokenApi(response.success.token);
          ApplicationSettings.setString(
            "userProfile",
            JSON.stringify(response.success)
          );
          ApplicationSettings.setString(
            "possibleFingerprintAuth",
            JSON.stringify(firstAccess)
        );
        ApplicationSettings.remove("registrationSteps");
        this.utilsService.goToScreen("/dashboard");
        }
      });
  }

  /**
   * Verifica a permissão sugere a permissão
   * nas configurações se necessário.
   */
  public verifyPermission() {
    if (!hasCameraPermissions()) {
      requestCameraPermissions().then(hasPermission => {
        if (!hasPermission) {
          permissionsCore.openAppSettings();
        }
      });
    }
  }

}
