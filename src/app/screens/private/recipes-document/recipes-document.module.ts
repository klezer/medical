import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { RecipesDocumentRoutingModule } from "./recipes-document-routing.module";
import { RecipesDocumentComponent } from "./recipes-document.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
/** Diretivas */
import { MedicineItemModule } from "./../../../directives/medicine-item/medicine-item.module";
@NgModule({
    imports: [
        HttpClientModule,
        NativeScriptCommonModule,
        RecipesDocumentRoutingModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
        }),
        MedicineItemModule
    ],
    declarations: [
        RecipesDocumentComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        RecipesDocumentComponent
    ]
})
export class RecipesDocumentModule { }
