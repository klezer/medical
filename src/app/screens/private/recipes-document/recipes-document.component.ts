import { Component  } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { ClientService } from "./../../../services";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page";
import { isAndroid } from "tns-core-modules/platform";
import { RouterExtensions } from "nativescript-angular/router";
import * as moment from 'moment'; 
@Component({
    selector: "RecipesDocument",
    moduleId: module.id,
    templateUrl: "./recipes-document.component.html",
    styleUrls: ['./recipes-document.component.scss']
})

export class RecipesDocumentComponent {
    public delayTimer: any;
    public userProfile: any;
    public isAndroid = isAndroid;
    public loadedAsync: boolean = false;
    public recipe: any = [];
    public recipeID: any;
    public routers = {
        getRecipe: 'doctor-recipe-patient',
    }
    public moment = moment;

    constructor(private ActivatedRoute: ActivatedRoute,
                private clientService: ClientService,
                public page: Page,
                private routerExtensions: RouterExtensions){
        /** Remove o Action Bar padrão */            
        this.page.actionBarHidden = true;
        /** Resgata os dados do usuário logado */
        this.userProfile = JSON.parse(ApplicationSettings.getString('userProfile'));
         /** Recebe os parâmetros da rota */
         this.ActivatedRoute.params.forEach((params) => {
            this.recipeID = params.recipe_id;
        });
    }

    ngAfterContentInit(): void {
        this.loadedConfirm();
    }

    /**
     * confirmação de carregamento da API.
     */
    public loadedConfirm() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.getRecipeInfo();
        }, 450);
    }

    /**
     * Pega as informações da receita.
     */
    public getRecipeInfo() {
        this.clientService.getById(this.routers.getRecipe, this.recipeID).then(response => {
            this.recipe = response;
            this.loadedAsync = true;
        });
    }

    /**
     * Realiza a replicação da receita.
     */
    public replicateRecipe() {
        this.routerExtensions.navigate([`/new-recipe/${this.recipe.medical_patient.id}/${this.recipeID}`], {
            // clearHistory: true,
            transition: {
                name: "slideTop",
            }
        });
    }

}