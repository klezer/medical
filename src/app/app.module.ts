import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
/** Menu Lateral */
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
/** Modais */
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { StartTratmentModule } from "./screens/private/recipes/actions/start-tratment/start-tratment.module";
/** Diretivas */
import { MenuModule } from './directives/menu/menu.module';
import { ContactUserModule } from './directives/contact-user/contact-user.module';
import { RecipeItemModule } from './directives/recipe-item/recipe-item.module';
import { MaskedTextFieldModule } from "nativescript-masked-text-field/angular";
import { TNSImageCacheItModule } from "nativescript-image-cache-it/angular";
@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptUISideDrawerModule,
        MaskedTextFieldModule,
        TNSImageCacheItModule,
        StartTratmentModule,
        MenuModule,
        ContactUserModule,
        RecipeItemModule
    ],
    declarations: [
        AppComponent
    ],
    entryComponents: [
    ],
    providers:[
        ModalDialogService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
