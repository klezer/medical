import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TextField } from "tns-core-modules/ui/text-field";

@Component({
    moduleId: module.id,
    selector: 'search-top-bar, [search-top-bar]',
    templateUrl: './search-top-bar.component.html',
    styleUrls: ['./search-top-bar.component.scss']
})

export class SearchTopBarComponent {
    @Input('placeholder') placeholder?: string;
    @Input('enabled') enabled?: boolean;
    @Input('icon') icon: string;
    @Input('icon-position') iconPosition: string;
    @Output() search: EventEmitter<any> = new EventEmitter();
    public searchInfo: string;
    public delayTimer: any = null;

    constructor() { }

    /**
     * Realiza a busca do texto informado.
     */
    public searchText(args) {
        let textField = <TextField>args.object;
        this.searchInfo = textField.text;
        this.delaySearch();
        this.placeholder = (this.placeholder) ? this.placeholder : 'Pesquisar';
    }

    /**
     * Realiza o atraso necessário na busca.
     */
    public delaySearch() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            this.search.emit(encodeURI(this.searchInfo));
        }, 500);
    }

}
