import { Component, Input } from '@angular/core';
import { AbsoluteLayout } from "tns-core-modules/ui/layouts/absolute-layout"

@Component({
    moduleId: module.id,
    selector: 'medicine-item, [medicine-item]',
    templateUrl: './medicine-item.component.html',
    styleUrls: ['./medicine-item.component.scss']
})

export class MedicineItemComponent {
    public delay: any;
    @Input('item') item: any;
    @Input('index') index?: any;
    @Input('itens') itens?: any;

    constructor() {
        
    }

    ngAfterContentInit(): void {
        this.index = (this.index) ? this.index : 0;
    }

    /**
     * Verifica a largura do container para ajustar o status do andamento.
     */
    public getWidth(args, item) {
        clearTimeout(this.delay);
        this.delay = setTimeout(() => {
            let count = Math.abs(item.time_line);
            let width = (<AbsoluteLayout>args.object).getActualSize().width / 100;
            item.width = width * count;
        }, 150);
    }

}
