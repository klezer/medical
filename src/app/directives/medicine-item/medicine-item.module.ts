import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MedicineItemComponent } from "./medicine-item.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule,
        TNSFontIconModule.forRoot({
			"icomoon": "./fonts/icon-font-icomoon.css"
        })
    ],
    declarations: [
        MedicineItemComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MedicineItemComponent
    ]
})
export class MedicineItemModule { }
