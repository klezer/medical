import { Component, AfterContentChecked } from '@angular/core';
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { NavigationEnd, Router } from "@angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { filter } from "rxjs/operators";
import { ClientService } from "./../../services";
import * as app from "tns-core-modules/application";
import { screen, isAndroid } from "tns-core-modules/platform";

@Component({
    moduleId: module.id,
    selector: 'menu, [menu]',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements AfterContentChecked {
    public isAndroid = isAndroid;
    public userProfile: any = null;
    public menuList = [
        { name: 'Minhas Receitas', route: '/dashboard', icon: 'icomoon-paper', class: 'icomoon', badge: 0 },
        { name: 'Meus pacientes', route: '/patients', icon: 'icomoon-user', class: 'icomoon', badge: 0 },
        { name: 'Mensagens', route: '/messages', icon: 'icomoon-messages', class: 'icomoon', badge: 0 },
        { name: 'Convite', route: '/invite', icon: 'icomoon-add-user', class: 'icomoon', badge: 0 },
    ];
    private _activatedUrl: string = '/dashboard';
    public screen = screen;

    constructor(private router: Router,
                private clientService: ClientService) {
        this.monitorRouterEvents();
        this.getUserInfo();
    }

    ngAfterContentChecked():void {
        this.getUserInfo();
    }
    
    /**
     * Verifica o componente selecionado.
     */
    public isComponentSelected(url: string): boolean {
        return this._activatedUrl === url;
    }

    /**
     * Monitora os eventos da rota.
     */
    public monitorRouterEvents() {
        this.router.events
        .pipe(filter((event: any) => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => this._activatedUrl = event.urlAfterRedirects);
    }

    /**
     * Verifica e pega as informações do usuário recém logado.
     */
    public getUserInfo() {
        if (ApplicationSettings.hasKey("userProfile")) {
        this.userProfile = JSON.parse(
            ApplicationSettings.getString("userProfile")
        );
        }
    }

    /**
     * Sai do ambiente logado/privado.
     */
    public loggout() {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
        setTimeout(() => {
            this.clientService.loggout();
        }, 300);
    }

}