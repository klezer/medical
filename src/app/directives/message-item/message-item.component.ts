import { Component, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Page } from "tns-core-modules/ui/page";
import { ClientService, EventEmitterService } from "./../../services";
import { RadListView, ListViewEventData } from "nativescript-ui-listview";
import * as moment from 'moment'; 
import 'moment/min/locales';

@Component({
    moduleId: module.id,
    selector: 'message-item, [message-item]',
    templateUrl: './message-item.component.html',
    styleUrls: ['./message-item.component.scss']
})

export class MessageItemComponent {
    @Input('ready') readyToShow: boolean;
    @Input('height') height: any;
    @Input('messages') messages: any;
    @Input('item') item: any;
    @Input('name') name: any;
    @Output() pagination: EventEmitter<any> = new EventEmitter();

    public initialCount: number;
    public delayTimer: any;
    public delayNew: any;
    public countPage: number = 1;
    public lastPosition: number = 0;
    public routers = {
        lastComment: 'pusher-comment-update'
    }
    public newData = false;

    constructor(public page: Page,
                private clientService: ClientService) {   
         EventEmitterService.get("newMessage").subscribe({
            next: () => {
                this.newData = false;
            }
        });
    }

    /**
     * Confere se as mensagen já podem ser exibidas no chat.
     */
    ngOnChanges(changes: SimpleChanges) {
        // if(changes.readyToShow.currentValue) 
    }

    /**
     * Confirmação de visualização de comentário.
     */
    public previewConfirmation(ID) {
        let view = {
            comment_id: ID
        }
        this.clientService.postAll(this.routers.lastComment, view, false, ['error']);
    }

    /**
     * Formata a data exibindo a quanto tempo ocorrreu o comentário.
     */
    public formatDate(date) {
       return moment(moment.unix(parseInt(date)), 'YYYYMMDD').locale('pt-br').fromNow();
    }

    /**
     * Realiza a paginação da lista.
     */
    public onPullToRefreshInitiated(args: ListViewEventData) {
        const last = this.messages.list.length;
        this.lastPosition = last;
        this.newData = true;
        this.countPage = this.countPage + 1;
        this.pagination.emit(this.countPage);
        setTimeout(() => {
            (args.object).notifyPullToRefreshFinished();
            this.scrollTo();
        }, 1200);
    }

    /**
     * Realiza o scroll da lista.
     */
    public scrollTo(down = false, index = 0) {
        let position: any = "Start";
        if(!down){
            index = (this.messages.list.length <= 50) ? this.messages.list.length - 1 : 50;
        }
        setTimeout(() => {
            (<RadListView>this.page.getViewById("listView")).scrollToIndex(index, false, position);
        }, 0);
    }

}
