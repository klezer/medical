import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ModalComponent } from "./modal.component";
import { NativeScriptRouterModule } from "nativescript-angular/router";

@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule
    ],
    declarations: [
        ModalComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        ModalComponent
    ]
})
export class ModalModule { }
