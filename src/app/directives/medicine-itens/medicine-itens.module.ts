import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MedicineItensComponent } from "./medicine-itens.component";
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { DropDownModule } from "nativescript-drop-down/angular";
/** Diretivas */
import { SearchTopBarModule } from './../search-top-bar/search-top-bar.module';

@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptFormsModule,
        DropDownModule,
        SearchTopBarModule,
        TNSFontIconModule.forRoot({
            "icomoon": "./fonts/icon-font-icomoon.css"
        })
    ],
    declarations: [
        MedicineItensComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MedicineItensComponent
    ]
})
export class MedicineItensModule { }
