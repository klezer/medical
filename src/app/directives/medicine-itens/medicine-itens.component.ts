import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Slider } from "tns-core-modules/ui/slider";
import { ValueList, SelectedIndexChangedEventData } from "nativescript-drop-down";
import * as ModalPicker from "nativescript-modal-datetimepicker";
import * as moment from 'moment';
import { EventEmitterService } from "./../../services/index";

@Component({
    moduleId: module.id,
    selector: 'medicine-itens, [medicine-itens]',
    templateUrl: './medicine-itens.component.html',
    styleUrls: ['./medicine-itens.component.scss']
})

export class MedicineItensComponent {
    public searchInfo: string;
    public delayTimer: any = null;
    public formRecipe: any;
    public listInterval: Array<number> = [2, 4, 6, 8, 12, 24];
    public routers = {
        // retrieveAll: 'http://172.28.20.55:8001/api/list-medicamentos'
        retrieveAll: 'https://crawler-drpediu.doc88.com.br/api/list-medicamentos'
    };
    public medicineList = [];
    public selected = null;
    public showSearch: boolean = true;
    public dosages: ValueList<string>;
    public listDosage: any;
    public itemMedicine: any = {
        dosage: null,
        considerations: null,
        frequency: null,
        number_of_days: null,
        title: null,
        subtitle: null,
        note: '',
        id: null,
        opened: true
    };
    public canAdd: boolean;
    public select_dosage_value: number = 0;
    public picker: any;
   
    @Input('item') item: any;
    @Input('index') index: any;
    @Input('list') list: any;
    @Output() form: EventEmitter<any> = new EventEmitter();
    @Output() valid: EventEmitter<any> = new EventEmitter();

    constructor() { 
        this.listDosage = [
            { value: '', display: 'Unidades'}
        ];
        this.dosages =  new ValueList<string>(this.listDosage);
    }

    ngAfterContentInit(): void {
        this.canAdd = false;
        this.populateDosage();
        this.formControl();
        this.item.valid = this.formRecipe.valid;
    }

    /** 
     * Busca todos os registros 
     * */
    public getAllRecipes(router, obj) {
        this.getAll(router, obj).then((response: any) => {
            if(response.length > 0){
                console.dir('event-medicine-list');
                EventEmitterService.get("event-medicine-list").emit('true');
            }
            this.medicineList = response;
        });
    }

    /**
     * Abre o medicamento.
     */
    public openMedicine(item) {
        let index = this.list.map((child) => child).indexOf(item);
        this.closeItems(index);
        this.canAdd = false;
        this.select_dosage_value = this.formRecipe.get('dosage').value;
    }

    /**
     * Permite a exibição somente do item selecionado.
     */
    public closeItems(index){
        this.list.forEach((item: any, key) => {
            item.opened = false;
        });
        this.list[index].opened = true;
    }

    /**
     * Popula a quantidade de unidades.
     */
    public populateDosage() {
        let i = 1;
        for(; i <= 50; i++) {
            let unit = (i > 1) ? `${i} unidades` : `${i} unidade`;
            this.listDosage.push({ value: i, display: unit});
            if (i == 50) {
                this.dosages =  new ValueList<string>(this.listDosage);
            }
        };
    };

    /**
     * Define todo os campos do formuário 
     * com suas regras de validação.
     */
    private formControl() {
        this.item.dosage = (this.item.dosage != null || this.item.dosage != '') ? this.item.dosage : 1;
        /** Formulário do Medicamentos */
        this.formRecipe = new FormGroup({
            dosage: new FormControl(this.item.dosage, [Validators.required]),
            considerations: new FormControl(this.item.considerations, []),
            frequency: new FormControl(this.item.frequency, [Validators.required]),
            number_of_days: new FormControl(this.item.number_of_days, [Validators.required]),
            medicine_id: new FormControl(this.item.medicine_id, [Validators.required]),
            title: new FormControl(this.item.title, [Validators.required]),
            subtitle: new FormControl(this.item.subtitle, [Validators.required]),
            note: new FormControl((this.item.note) ? this.item.note : ''),
            validity_of_medicine: new FormControl(this.item.validity_of_medicine, [Validators.required])
        });

        this.replicateRecipe();
    }

    /**
     * Caso seja uma recplicação re receita.
     */
    public replicateRecipe() {
        if(this.item.id){
            this.selectMedicine(this.item);
            this.confirmMedicine(this.item, this.index);
        }
    }

    /**
     * Aciona o modal de datas.
     */
    public pickDate() {
        const picker = new ModalPicker.ModalDatetimepicker();
        picker.pickDate({
            title: 'Validade da receita',
            cancelLabel: 'Cancelar',
            doneLabel: 'Confirmar',
            theme: 'dark',
            is24HourView: false,
            minDate: new Date(),
            startingDate: new Date(moment().add(31, 'days').format("YYYY-MM-DD"))
        }).then((result) => {
            if(result) {
                let day = (result['day'] <= 9) ? '0'+result['day'] : result['day'];
                let month = (result['month'] <= 9) ? '0'+result['month'] : result['month'];
                this.picker = day + '/' + month + '/' + result['year'];
                this.formRecipe.controls['validity_of_medicine'].setValue(`${result['year']}-${month}-${day}`);
            }
        });
    }

    /**
     * Seleciona o medicamento e verifica 
     * se pode ser adicionado. 
     */
    public selectMedicine(item) {
        this.selected = JSON.parse(JSON.stringify(item));
        let index = this.list.map((child) => child.medicine_id).indexOf(this.selected.id);
        if (index == -1 || item.id) {
            this.formRecipe.controls['medicine_id'].setValue(this.selected.id);
            this.formRecipe.controls['title'].setValue(this.selected.titulo);
            this.formRecipe.controls['subtitle'].setValue(this.selected.subtitulo);
            this.formRecipe.controls['note'].setValue((this.item.id) ? this.item.note : this.selected.descricao);
            this.medicineList = null;
            this.showSearch = false;
            this.picker = moment().add(item.days_validity, 'days').format("DD/MM/YYYY");
            this.formRecipe.controls['validity_of_medicine'].setValue(moment().add(item.days_validity + 1, 'days').format("YYYY-MM-DD"));
        }

    }

    /**
     * Remove o medicamento.
     */
    public removeMedicine() {
        this.formRecipe.controls['medicine_id'].setValue('');
        this.formRecipe.controls['title'].setValue('');
        this.formRecipe.controls['subtitle'].setValue('');
        this.formRecipe.controls['note'].setValue('');
        this.selected = null;
        this.showSearch = true;
    }

    /**
     * Adiciona um novo medicamento a receita.
     */
    public confirmMedicine(item, index) {
        item.opened = false;
        // Ajusta o valor do período.
        this.formRecipe.controls['number_of_days'].setValue(Math.round(this.formRecipe.get('number_of_days').value));
        // objeto de saída.
        let medicine = {
            index: index,
            new: true,
            value: this.formRecipe.value,
            for_interaction: this.selected.principio_ativo ? this.selected.principio_ativo : this.selected.titulo
        }
        this.form.emit(medicine);
        this.canAdd = true;
    }

    /**
     * Adiciona um novo medicamento a receita.
     */
    public newMedicine() {
        this.list.push(this.itemMedicine);
        this.valid.emit(false);
    }

    /**
     * Remove o medicamento da receita.
     */
    public removeItem(item) {
        if(this.list.length > 1) { 
            let index = this.list.map((child) => child).indexOf(item);
            // objeto de saída.
            let medicine = {
                index: index,
                new: false,
                value: null
            }
            this.form.emit(medicine);
        }
    }

     /**
     * Observa as mudanças do componete de selecão e atribui os valores.
     */
    public changeSelect(args: SelectedIndexChangedEventData) {
        this.formRecipe.controls['dosage'].setValue(this.listDosage[args.newIndex].value);
    }

    /**
     * Realiza a consulta das receitas.
     */
    public search(searchInfo) {
        this.getAllRecipes(this.routers.retrieveAll, searchInfo);
    }

    /**
     * Monitora o valor do slider de período.
     */
    public onSliderValueChange(args) {
        let slider = <Slider>args.object;
        this.item.number_of_days = slider;
    }

    /**
     * Seleciona o intervalo de dias.
     */
    public selectInterval(value) {
        this.formRecipe.controls['frequency'].setValue(value);
    }

  /**
   * Busca todaos as medicamentos.
   */
  public getAll(route, obj) {
    return new Promise((resolve, reject) => {
      fetch(`${route}/${obj}`, {
        method: "GET",
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        }),
      })
      .then((r) => r.json())
        .then((response) => {
          const finalResponse = response.data ? response.data : response;
          resolve(finalResponse);
        })
        .catch((e) => {
          reject(new Error(e.message))
        });
      });
  }

}
