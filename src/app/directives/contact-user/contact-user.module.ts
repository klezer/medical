import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ContactUserComponent } from "./contact-user.component";

@NgModule({
    imports: [
        NativeScriptRouterModule,
        NativeScriptCommonModule,
        TNSFontIconModule.forRoot({
            "mdi": "./fonts/icon-font-material-design-icons.css",
            "fa": "./fonts/icon-font-awesome.css",
			"icomoon": "./fonts/icon-font-icomoon.css"
        })
    ],
    declarations: [
        ContactUserComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        ContactUserComponent
    ]
})
export class ContactUserModule { }
