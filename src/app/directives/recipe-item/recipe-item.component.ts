import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'recipe-item, [recipe-item]',
    templateUrl: './recipe-item.component.html',
    styleUrls: ['./recipe-item.component.scss']
})

export class RecipeItemComponent {
    @Input('item') item: any;
    @Input('link') link: any;
    @Input('image-or-icon') type: any;

    constructor() { }

}
