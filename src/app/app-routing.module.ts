import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: "~/app/screens/public/home/home.module#HomeModule" },
    { path: "register", loadChildren: "~/app/screens/public/register/register.module#RegisterModule" },
    {
        path: "register-image",
        loadChildren:
          "~/app/screens/private/register-image/register-image.module#RegisterImageModule"
      },
      {
        path: "register-image/:user_id",
        loadChildren:
          "~/app/screens/private/register-image/register-image.module#RegisterImageModule"
      },
    { path: "terms", loadChildren: "~/app/screens/public/terms/terms.module#TermsModule" },
    { path: "dashboard", loadChildren: "~/app/screens/private/recipes/recipes.module#RecipesModule" },
    { path: "new-recipe/:patient_id/:recipe_id", loadChildren: "~/app/screens/private/new-recipe/new-recipe.module#NewRecipeModule"},
    { path: "new-recipe/:patient_id", loadChildren: "~/app/screens/private/new-recipe/new-recipe.module#NewRecipeModule"},
    { path: "anamnesis/:patient_id", loadChildren: "~/app/screens/private/anamnesis/anamnesis.module#AnamnesisModule" },
    { path: "recipes-document/:recipe_id", loadChildren: "~/app/screens/private/recipes-document/recipes-document.module#RecipesDocumentModule"},
    { path: "my-profile", loadChildren: "~/app/screens/private/my-profile/my-profile.module#MyProfileModule" },
    { path: "patients", loadChildren: "~/app/screens/private/patients/patients.module#PatientsModule" },
    { path: "patients-details/:patient_id", loadChildren: "~/app/screens/private/patients-details/patients-details.module#PatientsDetailsModule"},
    { path: "messages", loadChildren: "~/app/screens/private/messages/messages.module#MessagesModule" },
    { path: "messages-details/:recipe_id/:patient_id", loadChildren: "~/app/screens/private/messages-details/messages-details.module#MessagesDetailsModule"},
    { path: "invite", loadChildren: "~/app/screens/private/invite/invite.module#InviteModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
