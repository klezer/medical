import { Injectable } from "@angular/core";
import { EventEmitterService } from "./../index";
import { Feedback, FeedbackType } from "nativescript-feedback";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";
import { Color } from "tns-core-modules/color/color";
import { isString } from "tns-core-modules/utils/types";

export interface Notification {
  type: FeedbackType; //success | info | warning | error
  title?: string;
  titleColor?: Color;
  message: string;
}

export interface AlertDefault {
  type: string; //showSuccess | showError | showNotice | showWarning | showInfo
  title: string;
  message?: string;
  button?: string;
}

export interface AlertButtons {
  title: string;
  message?: string;
  selectBtns: Array<string>;
}

@Injectable({
  providedIn: "root"
})
export class NotificationService {
  public feedback: Feedback;
  constructor() {
  }

  /**
   * Exibe o feedback para o usuário.
   * @param Notification
   */
  public showFeedback(Notification) {
    this.feedback = new Feedback();
    if (Notification.type === "error" || Notification.type === "errors" || Notification.type === "message") {
      Notification.message = this.errorHandling(Notification.message);
    }
    this.feedback[Notification.type](Notification);
  }

  /**
   * Oculta o feedback para o usuário.
   */
  public hideFeedback() {
    this.feedback.hide();
  }

  /**
   * Exibe o modal de alerta padrão para o usuário.
   */
  public showAlert(AlertDefault) {
    if (AlertDefault.type === "showInfo") {
      TNSFancyAlert.customViewColor = "#005CB9";
    }
    TNSFancyAlert[AlertDefault.type](
      AlertDefault.title,
      AlertDefault.message,
      AlertDefault.button
    );
  }

  /**
   * Exibe o modal de alerta com botões e emissão de evento.
   */
  public showAlertButton(AlertButtons) {
    let buttons = [];
    AlertButtons.selectBtns.forEach(item => {
      buttons.push(
        new TNSFancyAlertButton({
          label: item,
          action: () => {
            EventEmitterService.get("showAlertButton").emit(item);
          }
        })
      );
    });
    TNSFancyAlert.showCustomButtons(
      buttons,
      undefined,
      undefined,
      AlertButtons.title,
      AlertButtons.message ? AlertButtons.message : null
    );
  }

  /**
   * Trata os erros para exibição.
   */
  public errorHandling(errors) {
    let result: string;
    let moreErrors = [];
    if (isString(errors)) {
      result = errors;
    } else {
      Object.keys(errors).forEach(field => {
        let error = errors[field];
        Object.keys(error).forEach(field => {
          let key = field;
          if (Array.isArray(error[key])) {
            error[key].forEach(msg => {
              moreErrors.push(msg);
            });
            result = moreErrors.length > 1 ? moreErrors.join() : moreErrors[0];
          } else {
            result = error[key];
          }
        });
      });
    }
    return result;
  }
}
