import { Observable } from 'tns-core-modules/data/observable';
import { Pusher } from 'nativescript-pusher';
import { ConnectionStatusEvent } from 'nativescript-pusher';

export class PusherService extends Observable {
  public channelName: any;
  public eventName: any;
  public messages: any;
  public pusher: Pusher;
  
  constructor() {
    super();
    this.pusher = new Pusher('a74dc8dc7f91a7b08687', {
      cluster: 'us2'
    });
  }

  /**
   * Conecta ao serviço do Pusher.
   */
  public connect() {
    return new Promise((resolve, reject) => { 
      this.pusher.connect((error, status: ConnectionStatusEvent) => {
        if (error) {
          reject(error);
        } else {
          resolve(status);
        }
      });
    });
  }

  /**
   * Desconecta ao Pusher.
   */
  public disconnect() {
    return new Promise((resolve, reject) => { 
      this.channelName = '';
      this.eventName = '';        
      this.messages = '';
      this.pusher.disconnect();
      resolve(true);
    });
  }

}
