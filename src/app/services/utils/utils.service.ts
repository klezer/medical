import { Injectable } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as utils from "tns-core-modules/utils/utils";
import * as frame from "tns-core-modules/ui/frame";
import { isAndroid, isIOS } from "tns-core-modules/platform";

@Injectable({
  providedIn: "root"
})
export class UtilsService {
  constructor(private routerExtensions: RouterExtensions) {}

  /**
   * Envia para a tela desejada.
   */
  public goToScreen(route) {
    this.routerExtensions.navigate([route], {
      clearHistory: true,
      transition: {
        name: "fade"
      }
    });
  }

/**
 * Fecha o teclado.
 */
  public dismissSoftKeybaord() {
    if (isIOS) frame.topmost().nativeView.endEditing(true);
    if (isAndroid) utils.ad.dismissSoftInput();
  }
}
