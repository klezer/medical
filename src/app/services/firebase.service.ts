import { Injectable } from "@angular/core";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import { Observable } from "tns-core-modules/data/observable";
import { messaging } from "nativescript-plugin-firebase/messaging";
import { LocalNotifications } from "nativescript-local-notifications";
import { Color } from "tns-core-modules/color";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import { RouterExtensions } from "nativescript-angular/router";
import { ClientService, EventEmitterService } from "./index";
const firebase = require("nativescript-plugin-firebase");
const model = (isIOS) ? new messaging.PushNotificationModel() : null;

@Injectable({
  providedIn: "root"
})

export class FirebaseService extends Observable {
  public Firebase = firebase;
  public Model = model;
  public message: any;
  public badges: any = {
      myRecipes: 0,
      myPatients: 0,
      myMessages: 0
  };
  public hasKey: boolean = false;
  public notificationData: any;
  public routersList = {
      recipe: '/recipes',
      patient: '/patients'
  };

  constructor(private routerExtensions: RouterExtensions,
              private clientService: ClientService) { 
    super();
    this.clientService.isLogged().then(() => {
      this.doCheckHasPermission();
    });
  }

/**
 * Verifica se tem permissão para notificações.
 */
public doCheckHasPermission(): void {
  LocalNotifications.hasPermission()
      .then((granted: boolean) => {
        ApplicationSettings.setBoolean('hasPersmissionNotifications', granted);
        if(!granted) {
          this.doRequestPermission();
        }
      });
};

/**
 * Solicita a permissão do usuário para notificações.
 */
public doRequestPermission(): void {
  LocalNotifications.requestPermission();
}

  /**
   * Conecta ao seviço do Firebase.
   */
  public init() {
    return new Promise((resolve, reject) => { 
    this.Firebase.init({
      showNotifications: true,
      showNotificationsWhenInForeground: true,
      onPushTokenReceivedCallback: function(token) {
        ApplicationSettings.setString('firebaseToken', token);
      },
      onMessageReceivedCallback: (message: any) => {
        console.dir(`onMessageReceivedCallback Chegou no: ${(isAndroid) ? 'Android' : 'iOS'}`);
        this.clientService.isLogged().then(() => {
          this.notificationData = message.data;
          if (this.notificationData || this.notificationData != null) {
            if (this.notificationData.category) {
              this.categoryNotification(this.notificationData);
            }
          if(isIOS) this.iosActionsNotification();
          }
        })
      }
    }).then((response) => {
        // Trata a notificação no Android se clicada
        LocalNotifications.addOnMessageReceivedCallback((notification: any) => {
          this.redirectForAction();
        });
        resolve(response);
      },error => {
        reject(error);
      }
    );
    });
  }

  /**
   * Pega o token do Aparelho
   */
  public getCurrentPushToken() {
    return new Promise((resolve, reject) => { 
      this.Firebase.getCurrentPushToken()
      .then((token: string) => {
        resolve(token);
      })
      .catch((error) => reject(error));
    });
  }

  /**
   * Trata as categorias para exibição correta
   */
  public categoryNotification(message) {
    EventEmitterService.get(`event-${this.notificationData.category}`).emit(message);
    if(ApplicationSettings.hasKey('roomMessagesActived') && 
      ApplicationSettings.getString('roomMessagesActived')) {
      if(ApplicationSettings.getString('roomMessagesActived') != message.recipe_id) 
        this.showLocalNotification(message);
    } else {
      this.showLocalNotification(message);
    }
  }

  /**
   * showLocalNotification
   */
  public showLocalNotification(message) {
    LocalNotifications.schedule(
      [{
        id: 1,
        title: message.title,
        subtitle: message.subtitle,
        body: message.body,
        icon: 'res://heart',
        thumbnail: message.thumbnail,
        bigTextStyle: true, // Allow more than 1 row of the 'body' text
        sound: "default",
        color: new Color("#55D3AE"),
        image: message.image,
        forceShowWhenInForeground: true,
        channel: (message.category ) ? message.category.replace(/[^a-z]/g, '') : 'default', // not that this is revealed in the notification tray when you longpress it on Android
        ticker: "Chat Special ticker text (Android only)"
      }])
  }

  /**
   * Ações existente na notificação para iOS.
   */
  public iosActionsNotification() {
    this.Model.iosSettings = new messaging.IosPushSettings();
    this.Model.iosSettings.badge = true;
    this.Model.iosSettings.alert = true;

    this.Model.iosSettings.interactiveSettings = new messaging.IosInteractivePushSettings();
    this.Model.iosSettings.interactiveSettings.actions = [
      {
        identifier: "OPEN_ACTION",
        title: "Open the app (if closed)",
        options: messaging.IosInteractiveNotificationActionOptions.foreground
      },
      {
        identifier: "AUTH",
        title: "Open the app, but only if device is not locked with a passcode",
        options: messaging.IosInteractiveNotificationActionOptions.foreground | messaging.IosInteractiveNotificationActionOptions.authenticationRequired
      },
      {
        identifier: "INPUT_ACTION",
        title: "Toque para responder sem abrir o aplicativo",
        type: "input",
        submitLabel: "Fire!",
        placeholder: "Load the gun..."
      },
      {
        identifier: "INPUT_ACTION",
        title: "Toque para responder e abra o aplicativo",
        options: messaging.IosInteractiveNotificationActionOptions.foreground,
        type: "input",
        submitLabel: "Ok",
        placeholder: "Responder..."
      },
      {
        identifier: "DELETE_ACTION",
        title: "Excluir sem abrir o aplicativo",
        options: messaging.IosInteractiveNotificationActionOptions.destructive
      }
    ];

    this.Model.iosSettings.interactiveSettings.categories = [{
      identifier: "GENERAL"
    }];

    this.Model.onNotificationActionTakenCallback = (actionIdentifier: string, message: any) => {
      this.redirectForAction();
    };

    this.Firebase.registerForInteractivePush(model);
  }

  /**
   * Realiza o filtro de acordo com a categoria.
   */
  public redirectForAction() {
    if(this.notificationData && this.notificationData.category) {
      if(this.notificationData.category == 'chat') this.goToMessageRoom(this.notificationData.recipe_id, this.notificationData.patient_id);
    }
  }

  /**
   * Abre a tela de mensagens da receita com o usuário.
   */
  public goToMessageRoom(recipe_id, person_id) {
    this.routerExtensions.navigate([`/messages-details/${recipe_id}/${person_id}`], {
      transition: {
          name: "slideTop",
      }
    });
  }

}
