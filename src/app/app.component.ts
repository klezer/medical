import { Component, OnInit } from "@angular/core";
import {
  FirebaseService,
  NotificationService
} from "./services";
import { isAndroid } from "tns-core-modules/platform";
import * as ApplicationSettings from "tns-core-modules/application-settings";
import * as app from "tns-core-modules/application";
import { registerElement } from "nativescript-angular/element-registry";
import { CardView } from "nativescript-cardview";
// import { Gif } from "nativescript-gif";
import * as connectivity from "tns-core-modules/connectivity";
import { Color } from "tns-core-modules/color";

registerElement(
  "PreviousNextView",
  () => require("nativescript-iqkeyboardmanager").PreviousNextView
);
registerElement("CardView", () => CardView);
// registerElement("Gif", () => Gif);

@Component({
  moduleId: module.id,
  selector: "ns-app",
  templateUrl: "./app.component.html",
  providers: [FirebaseService]
})
export class AppComponent implements OnInit {
  public userLogged: boolean = false;
  public currentMessage: any;
  public opened: boolean = false;
  constructor(
    private firebaseService: FirebaseService,
    private notificationService: NotificationService
  ) {
    this.initialConnection();
    // Monitora a conexão do aparelho
    connectivity.startMonitoring((newConnectionType: number) => {
      this.connectionMonitor(newConnectionType);
    });
    this.androidCustomized();
    // Limpa a sala se houver ocupada
    if (ApplicationSettings.hasKey("roomMessagesActived")) {
      ApplicationSettings.remove("roomMessagesActived");
    }
  }

  ngOnInit(): void {
    let connectionType = ApplicationSettings.hasKey("connectionStatusType")
      ? ApplicationSettings.getNumber("connectionStatusType")
      : 0;
    // Se houver conexão com a internet
    if (connectionType > 0 && connectionType < 4) {
      this.firebaseService.init().then((response: any) => {
        this.currentMessage = response;
        this.firebaseService.getCurrentPushToken().then((response: any) => {
          console.dir(response + " TOKEN");
          ApplicationSettings.setString("deviceToken", response);
        });
      });
    }
  }

  /**
   * Verifica se há algum de conexão de dados ativa no aparelho.
   */
  public initialConnection() {
    let connectionType = connectivity.getConnectionType();
    ApplicationSettings.setNumber("connectionStatusType", connectionType);
    if (connectionType == 0) {
      this.notificationService.showFeedback({
        type: "error",
        message: "Por favor, conecte a internet."
      });
    }
  }
  /**
   * Monitora a conexão de dados do aparelho.
   */
  public connectionMonitor(newConnectionType) {
    if (newConnectionType == 0) {
      this.notificationService.showFeedback({
        type: "warning",
        message: "Por favor, conecte a internet."
      });
    }
  }

  /**
   * Customiza a barras de status e navegação para Android.
   */
  public androidCustomized() {
    if (isAndroid) {
      let window = app.android.startActivity.getWindow();
      // definição de color para barra de status
      window.setStatusBarColor(new Color("#439885").android);
      // definição de color para botoões de navegação
      window.setNavigationBarColor(new Color("#439885").android);

      let decorView = window.getDecorView();
      let View = android.view.View;
      decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }
  }

  /**
   * Monitora a abertura do Menu.
   */
  public drawerStatus(type) {
    this.opened = type;
  }
}
